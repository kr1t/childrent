<?php

use Illuminate\Database\Seeder;
use App\ProductImage;

class ProductImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductImage::insert(
            [
                [
                    'file_url' => '/images/product/stroller/s1.1.jpg',
                    'product_id' => 1
                ],
                [
                    'file_url' => '/images/product/stroller/s1.2.jpg',
                    'product_id' => 1
                ],
                [
                    'file_url' => '/images/product/stroller/s1.3.jpg',
                    'product_id' => 1
                ],
                [
                    'file_url' => '/images/product/stroller/s1.4.jpg',
                    'product_id' => 1
                ],

                [
                    'file_url' => '/images/product/stroller/s2.1.jpg',
                    'product_id' => 2
                ],
                [
                    'file_url' => '/images/product/stroller/s2.2.jpg',
                    'product_id' => 2
                ],
                [
                    'file_url' => '/images/product/stroller/s2.3.jpg',
                    'product_id' => 2
                ],
                [
                    'file_url' => '/images/product/stroller/s2.4.jpg',
                    'product_id' => 2
                ],
                [
                    'file_url' => '/images/product/stroller/s2.5.jpg',
                    'product_id' => 2
                ],

                [
                    'file_url' => '/images/product/stroller/s3.1.jpg',
                    'product_id' => 3
                ],
                [
                    'file_url' => '/images/product/stroller/s3.2.jpg',
                    'product_id' => 3
                ],
                [
                    'file_url' => '/images/product/stroller/s3.3.jpg',
                    'product_id' => 3
                ],
                [
                    'file_url' => '/images/product/stroller/s3.4.jpg',
                    'product_id' => 3
                ],
                [
                    'file_url' => '/images/product/stroller/s3.5.jpg',
                    'product_id' => 3
                ],

                [
                    'file_url' => '/images/product/stroller/s4.1.jpg',
                    'product_id' => 4
                ],
                [
                    'file_url' => '/images/product/stroller/s4.2.jpg',
                    'product_id' => 4
                ],
                [
                    'file_url' => '/images/product/stroller/s4.3.jpg',
                    'product_id' => 4
                ],
                [
                    'file_url' => '/images/product/stroller/s4.4.jpg',
                    'product_id' => 4
                ],
                [
                    'file_url' => '/images/product/stroller/s4.5.jpg',
                    'product_id' => 4
                ],

                [
                    'file_url' => '/images/product/stroller/s5.1.jpg',
                    'product_id' => 5
                ],
                [
                    'file_url' => '/images/product/stroller/s5.2.jpg',
                    'product_id' => 5
                ],
                [
                    'file_url' => '/images/product/stroller/s5.3.jpg',
                    'product_id' => 5
                ],
                [
                    'file_url' => '/images/product/stroller/s5.4.jpg',
                    'product_id' => 5
                ],

                [
                    'file_url' => '/images/product/stroller/s6.1.jpg',
                    'product_id' => 6
                ],
                [
                    'file_url' => '/images/product/stroller/s6.2.jpg',
                    'product_id' => 6
                ],
                [
                    'file_url' => '/images/product/stroller/s6.3.jpg',
                    'product_id' => 6
                ],
                [
                    'file_url' => '/images/product/stroller/s6.4.jpg',
                    'product_id' => 6
                ],
                [
                    'file_url' => '/images/product/stroller/s6.5.jpg',
                    'product_id' => 6
                ],

                [
                    'file_url' => '/images/product/stroller/s7.1.jpg',
                    'product_id' => 7
                ],
                [
                    'file_url' => '/images/product/stroller/s7.2.jpg',
                    'product_id' => 7
                ],
                [
                    'file_url' => '/images/product/stroller/s7.3.jpg',
                    'product_id' => 7
                ],
                [
                    'file_url' => '/images/product/stroller/s7.4.jpg',
                    'product_id' => 7
                ],
                [
                    'file_url' => '/images/product/stroller/s7.5.jpg',
                    'product_id' => 7
                ],

                [
                    'file_url' => '/images/product/stroller/s8.1.jpg',
                    'product_id' => 8
                ],
                [
                    'file_url' => '/images/product/stroller/s8.2.jpg',
                    'product_id' => 8
                ],
                [
                    'file_url' => '/images/product/stroller/s8.3.jpg',
                    'product_id' => 8
                ],

                [
                    'file_url' => '/images/product/stroller/s9.1.jpg',
                    'product_id' => 9
                ],
                [
                    'file_url' => '/images/product/stroller/s9.2.jpg',
                    'product_id' => 9
                ],
                [
                    'file_url' => '/images/product/stroller/s9.3.jpg',
                    'product_id' => 9
                ],
                [
                    'file_url' => '/images/product/stroller/s9.4.jpg',
                    'product_id' => 9
                ],

                [
                    'file_url' => '/images/product/stroller/s10.1.jpg',
                    'product_id' => 10
                ],
                [
                    'file_url' => '/images/product/stroller/s10.2.jpg',
                    'product_id' => 10
                ],
                [
                    'file_url' => '/images/product/stroller/s10.3.jpg',
                    'product_id' => 10
                ],

                [
                    'file_url' => '/images/product/stroller/s11.1.jpg',
                    'product_id' => 11
                ],
                [
                    'file_url' => '/images/product/stroller/s11.2.jpg',
                    'product_id' => 11
                ],
                [
                    'file_url' => '/images/product/stroller/s11.3.jpg',
                    'product_id' => 11
                ],
                [
                    'file_url' => '/images/product/stroller/s11.4.jpg',
                    'product_id' => 11
                ],
                [
                    'file_url' => '/images/product/stroller/s11.5.jpg',
                    'product_id' => 11
                ],

                [
                    'file_url' => '/images/product/stroller/s12.1.jpg',
                    'product_id' => 12
                ],
                [
                    'file_url' => '/images/product/stroller/s12.2.jpg',
                    'product_id' => 12
                ],
                [
                    'file_url' => '/images/product/stroller/s12.3.jpg',
                    'product_id' => 12
                ],
                [
                    'file_url' => '/images/product/stroller/s12.4.jpg',
                    'product_id' => 12
                ],

                [
                    'file_url' => '/images/product/stroller/s13.1.jpg',
                    'product_id' => 13
                ],
                [
                    'file_url' => '/images/product/stroller/s13.2.jpg',
                    'product_id' => 13
                ],
                [
                    'file_url' => '/images/product/stroller/s13.3.jpg',
                    'product_id' => 13
                ],
                [
                    'file_url' => '/images/product/stroller/s13.4.jpg',
                    'product_id' => 13
                ],

                [
                    'file_url' => '/images/product/stroller/s14.1.jpg',
                    'product_id' => 14
                ],
                [
                    'file_url' => '/images/product/stroller/s14.2.jpg',
                    'product_id' => 14
                ],
                [
                    'file_url' => '/images/product/stroller/s14.3.jpg',
                    'product_id' => 14
                ],
                [
                    'file_url' => '/images/product/stroller/s14.4.jpg',
                    'product_id' => 14
                ],
                [
                    'file_url' => '/images/product/stroller/s14.5.jpg',
                    'product_id' => 14
                ],

                [
                    'file_url' => '/images/product/stroller/s15.1.jpg',
                    'product_id' => 15
                ],
                [
                    'file_url' => '/images/product/stroller/s15.2.jpg',
                    'product_id' => 15
                ],
                [
                    'file_url' => '/images/product/stroller/s15.3.jpg',
                    'product_id' => 15
                ],
                [
                    'file_url' => '/images/product/stroller/s15.4.jpg',
                    'product_id' => 15
                ],
                [
                    'file_url' => '/images/product/stroller/s15.5.jpg',
                    'product_id' => 15
                ],

                [
                    'file_url' => '/images/product/stroller/s16.1.jpg',
                    'product_id' => 16
                ],
                [
                    'file_url' => '/images/product/stroller/s16.2.jpg',
                    'product_id' => 16
                ],
                [
                    'file_url' => '/images/product/stroller/s16.3.jpg',
                    'product_id' => 16
                ],

                [
                    'file_url' => '/images/product/stroller/s17.1.jpg',
                    'product_id' => 17
                ],
                [
                    'file_url' => '/images/product/stroller/s17.2.jpg',
                    'product_id' => 17
                ],

                [
                    'file_url' => '/images/product/stroller/s18.1.jpg',
                    'product_id' => 18
                ],
                [
                    'file_url' => '/images/product/stroller/s18.2.jpg',
                    'product_id' => 18
                ],
                [
                    'file_url' => '/images/product/stroller/s18.3.jpg',
                    'product_id' => 18
                ],
                [
                    'file_url' => '/images/product/stroller/s18.4.jpg',
                    'product_id' => 18
                ],
                [
                    'file_url' => '/images/product/stroller/s18.5.jpg',
                    'product_id' => 18
                ],

                [
                    'file_url' => '/images/product/stroller/s19.1.jpg',
                    'product_id' => 19
                ],
                [
                    'file_url' => '/images/product/stroller/s19.2.jpg',
                    'product_id' => 19
                ],
                [
                    'file_url' => '/images/product/stroller/s19.3.jpg',
                    'product_id' => 19
                ],
                [
                    'file_url' => '/images/product/stroller/s19.4.jpg',
                    'product_id' => 19
                ],

                [
                    'file_url' => '/images/product/stroller/s20.1.jpg',
                    'product_id' => 20
                ],
                [
                    'file_url' => '/images/product/stroller/s20.2.jpg',
                    'product_id' => 20
                ],
                [
                    'file_url' => '/images/product/stroller/s20.3.jpg',
                    'product_id' => 20
                ],
                [
                    'file_url' => '/images/product/stroller/s20.4.jpg',
                    'product_id' => 20
                ],

                // คาร์ซีท
                [
                    'file_url' => '/images/product/carseat/c1.1.jpg',
                    'product_id' => 21
                ],
                [
                    'file_url' => '/images/product/carseat/c1.2.jpg',
                    'product_id' => 21
                ],
                [
                    'file_url' => '/images/product/carseat/c1.3.jpg',
                    'product_id' => 21
                ],
                [
                    'file_url' => '/images/product/carseat/c1.4.jpg',
                    'product_id' => 21
                ],

                [
                    'file_url' => '/images/product/carseat/c2.1.jpg',
                    'product_id' => 22
                ],
                [
                    'file_url' => '/images/product/carseat/c2.2.jpg',
                    'product_id' => 22
                ],
                [
                    'file_url' => '/images/product/carseat/c2.3.jpg',
                    'product_id' => 22
                ],
                [
                    'file_url' => '/images/product/carseat/c2.4.jpg',
                    'product_id' => 22
                ],

                [
                    'file_url' => '/images/product/carseat/c3.1.jpg',
                    'product_id' => 23
                ],
                [
                    'file_url' => '/images/product/carseat/c3.2.jpg',
                    'product_id' => 23
                ],
                [
                    'file_url' => '/images/product/carseat/c3.3.jpg',
                    'product_id' => 23
                ],
                [
                    'file_url' => '/images/product/carseat/c3.4.jpg',
                    'product_id' => 23
                ],

                [
                    'file_url' => '/images/product/carseat/c4.1.jpg',
                    'product_id' => 24
                ],
                [
                    'file_url' => '/images/product/carseat/c4.2.jpg',
                    'product_id' => 24
                ],
                [
                    'file_url' => '/images/product/carseat/c4.3.jpg',
                    'product_id' => 24
                ],
                [
                    'file_url' => '/images/product/carseat/c4.4.jpg',
                    'product_id' => 24
                ],

                [
                    'file_url' => '/images/product/carseat/c5.1.jpg',
                    'product_id' => 25
                ],
                [
                    'file_url' => '/images/product/carseat/c5.2.jpg',
                    'product_id' => 25
                ],
                [
                    'file_url' => '/images/product/carseat/c5.3.jpg',
                    'product_id' => 25
                ],
                [
                    'file_url' => '/images/product/carseat/c5.4.jpg',
                    'product_id' => 25
                ],

                [
                    'file_url' => '/images/product/carseat/c6.1.jpg',
                    'product_id' => 26
                ],
                [
                    'file_url' => '/images/product/carseat/c6.2.jpg',
                    'product_id' => 26
                ],
                [
                    'file_url' => '/images/product/carseat/c6.3.jpg',
                    'product_id' => 26
                ],
                [
                    'file_url' => '/images/product/carseat/c6.4.jpg',
                    'product_id' => 26
                ],
                [
                    'file_url' => '/images/product/carseat/c6.5.jpg',
                    'product_id' => 26
                ],

                [
                    'file_url' => '/images/product/carseat/c7.1.jpg',
                    'product_id' => 27
                ],
                [
                    'file_url' => '/images/product/carseat/c7.2.jpg',
                    'product_id' => 27
                ],
                [
                    'file_url' => '/images/product/carseat/c7.3.jpg',
                    'product_id' => 27
                ],
                [
                    'file_url' => '/images/product/carseat/c7.4.jpg',
                    'product_id' => 27
                ],
                [
                    'file_url' => '/images/product/carseat/c7.5.jpg',
                    'product_id' => 27
                ],

                [
                    'file_url' => '/images/product/carseat/c8.1.jpg',
                    'product_id' => 28
                ],
                [
                    'file_url' => '/images/product/carseat/c8.2.jpg',
                    'product_id' => 28
                ],
                [
                    'file_url' => '/images/product/carseat/c8.3.jpg',
                    'product_id' => 28
                ],
                [
                    'file_url' => '/images/product/carseat/c8.4.jpg',
                    'product_id' => 28
                ],

                [
                    'file_url' => '/images/product/carseat/c9.1.jpg',
                    'product_id' => 29
                ],
                [
                    'file_url' => '/images/product/carseat/c9.2.jpg',
                    'product_id' => 29
                ],
                [
                    'file_url' => '/images/product/carseat/c9.3.jpg',
                    'product_id' => 29
                ],
                [
                    'file_url' => '/images/product/carseat/c9.4.jpg',
                    'product_id' => 29
                ],

                [
                    'file_url' => '/images/product/carseat/c10.1.jpg',
                    'product_id' => 30
                ],
                [
                    'file_url' => '/images/product/carseat/c10.2.jpg',
                    'product_id' => 30
                ],
                [
                    'file_url' => '/images/product/carseat/c10.3.jpg',
                    'product_id' => 30
                ],
                [
                    'file_url' => '/images/product/carseat/c10.4.jpg',
                    'product_id' => 30
                ],

                [
                    'file_url' => '/images/product/carseat/c11.1.jpg',
                    'product_id' => 31
                ],
                [
                    'file_url' => '/images/product/carseat/c11.2.jpg',
                    'product_id' => 31
                ],
                [
                    'file_url' => '/images/product/carseat/c11.3.jpg',
                    'product_id' => 31
                ],
                [
                    'file_url' => '/images/product/carseat/c11.4.jpg',
                    'product_id' => 31
                ],

                [
                    'file_url' => '/images/product/carseat/c12.1.jpg',
                    'product_id' => 32
                ],
                [
                    'file_url' => '/images/product/carseat/c12.2.jpg',
                    'product_id' => 32
                ],
                [
                    'file_url' => '/images/product/carseat/c12.3.jpg',
                    'product_id' => 32
                ],
                [
                    'file_url' => '/images/product/carseat/c12.4.jpg',
                    'product_id' => 32
                ],
                [
                    'file_url' => '/images/product/carseat/c12.5.jpg',
                    'product_id' => 32
                ],

                [
                    'file_url' => '/images/product/carseat/c13.1.jpg',
                    'product_id' => 33
                ],
                [
                    'file_url' => '/images/product/carseat/c13.2.jpg',
                    'product_id' => 33
                ],
                [
                    'file_url' => '/images/product/carseat/c13.3.jpg',
                    'product_id' => 33
                ],
                [
                    'file_url' => '/images/product/carseat/c13.4.jpg',
                    'product_id' => 33
                ],

                [
                    'file_url' => '/images/product/carseat/c14.1.jpg',
                    'product_id' => 34
                ],
                [
                    'file_url' => '/images/product/carseat/c14.2.jpg',
                    'product_id' => 34
                ],
                [
                    'file_url' => '/images/product/carseat/c14.3.jpg',
                    'product_id' => 34
                ],
                [
                    'file_url' => '/images/product/carseat/c14.4.jpg',
                    'product_id' => 34
                ],
                [
                    'file_url' => '/images/product/carseat/c14.5.jpg',
                    'product_id' => 34
                ],

                [
                    'file_url' => '/images/product/carseat/c15.1.jpg',
                    'product_id' => 35
                ],
                [
                    'file_url' => '/images/product/carseat/c15.2.jpg',
                    'product_id' => 35
                ],
                [
                    'file_url' => '/images/product/carseat/c15.3.jpg',
                    'product_id' => 35
                ],
                [
                    'file_url' => '/images/product/carseat/c15.4.jpg',
                    'product_id' => 35
                ],
                [
                    'file_url' => '/images/product/carseat/c15.5.jpg',
                    'product_id' => 35
                ],

                [
                    'file_url' => '/images/product/carseat/c16.1.jpg',
                    'product_id' => 36
                ],
                [
                    'file_url' => '/images/product/carseat/c16.2.jpg',
                    'product_id' => 36
                ],
                [
                    'file_url' => '/images/product/carseat/c16.3.jpg',
                    'product_id' => 36
                ],
                [
                    'file_url' => '/images/product/carseat/c16.4.jpg',
                    'product_id' => 36
                ],
                [
                    'file_url' => '/images/product/carseat/c16.5.jpg',
                    'product_id' => 36
                ],

                [
                    'file_url' => '/images/product/carseat/c17.1.jpg',
                    'product_id' => 37
                ],
                [
                    'file_url' => '/images/product/carseat/c17.2.jpg',
                    'product_id' => 37
                ],
                [
                    'file_url' => '/images/product/carseat/c17.3.jpg',
                    'product_id' => 37
                ],
                [
                    'file_url' => '/images/product/carseat/c17.4.jpg',
                    'product_id' => 37
                ],

                [
                    'file_url' => '/images/product/carseat/c18.1.jpg',
                    'product_id' => 38
                ],
                [
                    'file_url' => '/images/product/carseat/c18.2.jpg',
                    'product_id' => 38
                ],
                [
                    'file_url' => '/images/product/carseat/c18.3.jpg',
                    'product_id' => 38
                ],
                [
                    'file_url' => '/images/product/carseat/c18.4.jpg',
                    'product_id' => 38
                ],

                [
                    'file_url' => '/images/product/carseat/c19.1.jpg',
                    'product_id' => 39
                ],
                [
                    'file_url' => '/images/product/carseat/c19.2.jpg',
                    'product_id' => 39
                ],
                [
                    'file_url' => '/images/product/carseat/c19.3.jpg',
                    'product_id' => 39
                ],
                [
                    'file_url' => '/images/product/carseat/c19.4.jpg',
                    'product_id' => 39
                ],
                [
                    'file_url' => '/images/product/carseat/c19.5.jpg',
                    'product_id' => 39
                ],

                [
                    'file_url' => '/images/product/carseat/c20.1.jpg',
                    'product_id' => 40
                ],
                [
                    'file_url' => '/images/product/carseat/c20.2.jpg',
                    'product_id' => 40
                ],
                [
                    'file_url' => '/images/product/carseat/c20.3.jpg',
                    'product_id' => 40
                ],
                [
                    'file_url' => '/images/product/carseat/c20.4.jpg',
                    'product_id' => 40
                ],
                [
                    'file_url' => '/images/product/carseat/c20.5.jpg',
                    'product_id' => 40
                ],

                [
                    'file_url' => '/images/product/carseat/c21.1.jpg',
                    'product_id' => 41
                ],
                [
                    'file_url' => '/images/product/carseat/c21.2.jpg',
                    'product_id' => 41
                ],
                [
                    'file_url' => '/images/product/carseat/c21.3.jpg',
                    'product_id' => 41
                ],
                [
                    'file_url' => '/images/product/carseat/c21.4.jpg',
                    'product_id' => 41
                ],
                [
                    'file_url' => '/images/product/carseat/c21.5.jpg',
                    'product_id' => 41
                ],

                [
                    'file_url' => '/images/product/carseat/c22.1.jpg',
                    'product_id' => 42
                ],
                [
                    'file_url' => '/images/product/carseat/c22.2.jpg',
                    'product_id' => 42
                ],
                [
                    'file_url' => '/images/product/carseat/c22.3.jpg',
                    'product_id' => 42
                ],
                [
                    'file_url' => '/images/product/carseat/c22.4.jpg',
                    'product_id' => 42
                ],
                [
                    'file_url' => '/images/product/carseat/c22.5.jpg',
                    'product_id' => 42
                ],

                [
                    'file_url' => '/images/product/carseat/c23.1.jpg',
                    'product_id' => 43
                ],
                [
                    'file_url' => '/images/product/carseat/c23.2.jpg',
                    'product_id' => 43
                ],
                [
                    'file_url' => '/images/product/carseat/c23.3.jpg',
                    'product_id' => 43
                ],
                [
                    'file_url' => '/images/product/carseat/c23.4.jpg',
                    'product_id' => 43
                ],
                [
                    'file_url' => '/images/product/carseat/c23.5.jpg',
                    'product_id' => 43
                ],

                [
                    'file_url' => '/images/product/carseat/c24.1.jpg',
                    'product_id' => 44
                ],
                [
                    'file_url' => '/images/product/carseat/c24.2.jpg',
                    'product_id' => 44
                ],
                [
                    'file_url' => '/images/product/carseat/c24.3.jpg',
                    'product_id' => 44
                ],
                [
                    'file_url' => '/images/product/carseat/c24.4.jpg',
                    'product_id' => 44
                ],
                [
                    'file_url' => '/images/product/carseat/c24.5.jpg',
                    'product_id' => 44
                ],

                [
                    'file_url' => '/images/product/carseat/c25.1.jpg',
                    'product_id' => 45
                ],
                [
                    'file_url' => '/images/product/carseat/c25.2.jpg',
                    'product_id' => 45
                ],
                [
                    'file_url' => '/images/product/carseat/c25.3.jpg',
                    'product_id' => 45
                ],
                [
                    'file_url' => '/images/product/carseat/c25.4.jpg',
                    'product_id' => 45
                ],
                [
                    'file_url' => '/images/product/carseat/c25.5.jpg',
                    'product_id' => 45
                ],




                // รถเข็น ชุดที่ 2

                [
                    'file_url' => '/images/product/stroller/s22.1.jpg',
                    'product_id' => 46
                ],
                [
                    'file_url' => '/images/product/stroller/s22.2.jpg',
                    'product_id' => 46
                ],
                [
                    'file_url' => '/images/product/stroller/s22.3.jpg',
                    'product_id' => 46
                ],
                [
                    'file_url' => '/images/product/stroller/s22.4.jpg',
                    'product_id' => 46
                ],

                [
                    'file_url' => '/images/product/stroller/s23.1.jpg',
                    'product_id' => 47
                ],
                [
                    'file_url' => '/images/product/stroller/s23.2.jpg',
                    'product_id' => 47
                ],
                [
                    'file_url' => '/images/product/stroller/s23.3.jpg',
                    'product_id' => 47
                ],

                [
                    'file_url' => '/images/product/stroller/s24.1.jpg',
                    'product_id' => 48
                ],
                [
                    'file_url' => '/images/product/stroller/s24.2.jpg',
                    'product_id' => 48
                ],
                [
                    'file_url' => '/images/product/stroller/s24.3.jpg',
                    'product_id' => 48
                ],
                [
                    'file_url' => '/images/product/stroller/s24.4.jpg',
                    'product_id' => 48
                ],
                [
                    'file_url' => '/images/product/stroller/s24.5.jpg',
                    'product_id' => 48
                ],

                [
                    'file_url' => '/images/product/stroller/s25.1.jpg',
                    'product_id' => 49
                ],
                [
                    'file_url' => '/images/product/stroller/s25.2.jpg',
                    'product_id' => 49
                ],
                [
                    'file_url' => '/images/product/stroller/s25.3.jpg',
                    'product_id' => 49
                ],

                [
                    'file_url' => '/images/product/stroller/s26.1.jpg',
                    'product_id' => 50
                ],
                [
                    'file_url' => '/images/product/stroller/s26.2.jpg',
                    'product_id' => 50
                ],
                [
                    'file_url' => '/images/product/stroller/s26.3.jpg',
                    'product_id' => 50
                ],
                [
                    'file_url' => '/images/product/stroller/s26.4.jpg',
                    'product_id' => 50
                ],

                [
                    'file_url' => '/images/product/stroller/s27.1.jpg',
                    'product_id' => 51
                ],
                [
                    'file_url' => '/images/product/stroller/s27.2.jpg',
                    'product_id' => 51
                ],
                [
                    'file_url' => '/images/product/stroller/s27.3.jpg',
                    'product_id' => 51
                ],
                [
                    'file_url' => '/images/product/stroller/s27.4.jpg',
                    'product_id' => 51
                ],

                [
                    'file_url' => '/images/product/stroller/s28.1.jpg',
                    'product_id' => 52
                ],
                [
                    'file_url' => '/images/product/stroller/s28.2.jpg',
                    'product_id' => 52
                ],
                [
                    'file_url' => '/images/product/stroller/s28.3.jpg',
                    'product_id' => 52
                ],
                [
                    'file_url' => '/images/product/stroller/s28.4.jpg',
                    'product_id' => 52
                ],

                [
                    'file_url' => '/images/product/stroller/s29.1.jpg',
                    'product_id' => 53
                ],
                [
                    'file_url' => '/images/product/stroller/s29.2.jpg',
                    'product_id' => 53
                ],
                [
                    'file_url' => '/images/product/stroller/s29.3.jpg',
                    'product_id' => 53
                ],
                [
                    'file_url' => '/images/product/stroller/s29.4.jpg',
                    'product_id' => 53
                ],

                [
                    'file_url' => '/images/product/stroller/s30.1.jpg',
                    'product_id' => 54
                ],
                [
                    'file_url' => '/images/product/stroller/s30.2.jpg',
                    'product_id' => 54
                ],
                [
                    'file_url' => '/images/product/stroller/s30.3.jpg',
                    'product_id' => 54
                ],

                [
                    'file_url' => '/images/product/stroller/s31.1.jpg',
                    'product_id' => 55
                ],
                [
                    'file_url' => '/images/product/stroller/s31.2.jpg',
                    'product_id' => 55
                ],
                [
                    'file_url' => '/images/product/stroller/s31.3.jpg',
                    'product_id' => 55
                ],
                [
                    'file_url' => '/images/product/stroller/s31.4.jpg',
                    'product_id' => 55
                ],

                [
                    'file_url' => '/images/product/stroller/s32.1.jpg',
                    'product_id' => 56
                ],
                [
                    'file_url' => '/images/product/stroller/s32.2.jpg',
                    'product_id' => 56
                ],
                [
                    'file_url' => '/images/product/stroller/s32.3.jpg',
                    'product_id' => 56
                ],
                [
                    'file_url' => '/images/product/stroller/s32.4.jpg',
                    'product_id' => 56
                ],

                [
                    'file_url' => '/images/product/stroller/s33.1.jpg',
                    'product_id' => 57
                ],
                [
                    'file_url' => '/images/product/stroller/s33.2.jpg',
                    'product_id' => 57
                ],
                [
                    'file_url' => '/images/product/stroller/s33.3.jpg',
                    'product_id' => 57
                ],
                [
                    'file_url' => '/images/product/stroller/s33.4.jpg',
                    'product_id' => 57
                ],

                [
                    'file_url' => '/images/product/stroller/s34.1.jpg',
                    'product_id' => 58
                ],
                [
                    'file_url' => '/images/product/stroller/s34.2.jpg',
                    'product_id' => 58
                ],

                [
                    'file_url' => '/images/product/stroller/s35.1.jpg',
                    'product_id' => 59
                ],
                [
                    'file_url' => '/images/product/stroller/s35.2.jpg',
                    'product_id' => 59
                ],

                [
                    'file_url' => '/images/product/stroller/s36.1.jpg',
                    'product_id' => 60
                ],
                [
                    'file_url' => '/images/product/stroller/s36.2.jpg',
                    'product_id' => 60
                ],

                [
                    'file_url' => '/images/product/stroller/s37.1.jpg',
                    'product_id' => 61
                ],
                [
                    'file_url' => '/images/product/stroller/s37.2.jpg',
                    'product_id' => 61
                ],

                [
                    'file_url' => '/images/product/stroller/s38.1.jpg',
                    'product_id' => 62
                ],
                [
                    'file_url' => '/images/product/stroller/s38.2.jpg',
                    'product_id' => 62
                ],
                [
                    'file_url' => '/images/product/stroller/s38.3.jpg',
                    'product_id' => 62
                ],
                [
                    'file_url' => '/images/product/stroller/s38.4.jpg',
                    'product_id' => 62
                ],

                [
                    'file_url' => '/images/product/stroller/s39.1.jpg',
                    'product_id' => 63
                ],
                [
                    'file_url' => '/images/product/stroller/s39.2.jpg',
                    'product_id' => 63
                ],

                [
                    'file_url' => '/images/product/stroller/s40.1.jpg',
                    'product_id' => 64
                ],
                [
                    'file_url' => '/images/product/stroller/s40.2.jpg',
                    'product_id' => 64
                ],
                [
                    'file_url' => '/images/product/stroller/s40.3.jpg',
                    'product_id' => 64
                ],
                [
                    'file_url' => '/images/product/stroller/s40.4.jpg',
                    'product_id' => 64
                ],

                [
                    'file_url' => '/images/product/stroller/s41.1.jpg',
                    'product_id' => 65
                ],
                [
                    'file_url' => '/images/product/stroller/s41.2.jpg',
                    'product_id' => 65
                ],
                [
                    'file_url' => '/images/product/stroller/s41.3.jpg',
                    'product_id' => 65
                ],



                //ของเล่น
                [
                    'file_url' => '/images/product/toy/t1.1.jpeg',
                    'product_id' => 66
                ],
                [
                    'file_url' => '/images/product/toy/t1.2.jpeg',
                    'product_id' => 66
                ],

                [
                    'file_url' => '/images/product/toy/t2.1.jpeg',
                    'product_id' => 67
                ],
                [
                    'file_url' => '/images/product/toy/t2.2.jpeg',
                    'product_id' => 67
                ],

                [
                    'file_url' => '/images/product/toy/t3.1.jpg',
                    'product_id' => 68
                ],

                [
                    'file_url' => '/images/product/toy/t4.1.jpg',
                    'product_id' => 69
                ],
                [
                    'file_url' => '/images/product/toy/t4.2.jpg',
                    'product_id' => 69
                ],
                [
                    'file_url' => '/images/product/toy/t4.3.jpg',
                    'product_id' => 69
                ],

                [
                    'file_url' => '/images/product/toy/t5.1.jpg',
                    'product_id' => 70
                ],
                [
                    'file_url' => '/images/product/toy/t5.2.jpg',
                    'product_id' => 70
                ],

                [
                    'file_url' => '/images/product/toy/t6.1.jpg',
                    'product_id' => 71
                ],
                [
                    'file_url' => '/images/product/toy/t6.2.jpg',
                    'product_id' => 71
                ],
                [
                    'file_url' => '/images/product/toy/t6.3.jpg',
                    'product_id' => 71
                ],
                [
                    'file_url' => '/images/product/toy/t6.4.jpg',
                    'product_id' => 71
                ],

                [
                    'file_url' => '/images/product/toy/t7.1.jpg',
                    'product_id' => 72
                ],
                [
                    'file_url' => '/images/product/toy/t7.2.jpg',
                    'product_id' => 72
                ],
                [
                    'file_url' => '/images/product/toy/t7.3.jpg',
                    'product_id' => 72
                ],
                [
                    'file_url' => '/images/product/toy/t7.4.jpg',
                    'product_id' => 72
                ],

                [
                    'file_url' => '/images/product/toy/t8.1.jpg',
                    'product_id' => 73
                ],
                [
                    'file_url' => '/images/product/toy/t8.2.jpg',
                    'product_id' => 73
                ],
                [
                    'file_url' => '/images/product/toy/t8.3.jpg',
                    'product_id' => 73
                ],
                [
                    'file_url' => '/images/product/toy/t8.4.jpg',
                    'product_id' => 73
                ],
                [
                    'file_url' => '/images/product/toy/t8.5.jpg',
                    'product_id' => 73
                ],

                [
                    'file_url' => '/images/product/toy/t9.1.jpg',
                    'product_id' => 74
                ],
                [
                    'file_url' => '/images/product/toy/t9.2.jpg',
                    'product_id' => 74
                ],
                [
                    'file_url' => '/images/product/toy/t9.3.jpg',
                    'product_id' => 74
                ],
                [
                    'file_url' => '/images/product/toy/t9.4.jpg',
                    'product_id' => 74
                ],

                [
                    'file_url' => '/images/product/toy/t10.1.jpg',
                    'product_id' => 75
                ],
                [
                    'file_url' => '/images/product/toy/t10.2.jpg',
                    'product_id' => 75
                ],

                [
                    'file_url' => '/images/product/toy/t11.1.jpg',
                    'product_id' => 76
                ],
                [
                    'file_url' => '/images/product/toy/t11.2.jpg',
                    'product_id' => 76
                ],

                [
                    'file_url' => '/images/product/toy/t12.1.jpg',
                    'product_id' => 77
                ],
                [
                    'file_url' => '/images/product/toy/t12.2.jpg',
                    'product_id' => 77
                ],
                [
                    'file_url' => '/images/product/toy/t12.3.jpg',
                    'product_id' => 77
                ],

                [
                    'file_url' => '/images/product/toy/t13.1.jpg',
                    'product_id' => 78
                ],
                [
                    'file_url' => '/images/product/toy/t13.2.jpg',
                    'product_id' => 78
                ],
                [
                    'file_url' => '/images/product/toy/t13.3.jpg',
                    'product_id' => 78
                ],

                [
                    'file_url' => '/images/product/toy/t14.1.jpg',
                    'product_id' => 79
                ],


                [
                    'file_url' => '/images/product/toy/t15.2.jpg',
                    'product_id' => 80
                ],


                [
                    'file_url' => '/images/product/toy/t16.1.jpg',
                    'product_id' => 81
                ],

                [
                    'file_url' => '/images/product/toy/t17.1.jpg',
                    'product_id' => 82
                ],

                [
                    'file_url' => '/images/product/toy/t18.1.jpg',
                    'product_id' => 83
                ],

                [
                    'file_url' => '/images/product/toy/t19.jpg',
                    'product_id' => 84
                ],
                [
                    'file_url' => '/images/product/toy/t20.jpg',
                    'product_id' => 85
                ],
                [
                    'file_url' => '/images/product/toy/t21.jpg',
                    'product_id' => 86
                ],
            ]










        );
    }
}