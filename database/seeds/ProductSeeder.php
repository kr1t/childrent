<?php

use Illuminate\Database\Seeder;
use App\Product;
use App\ProductMark;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // foreach($products as $product){

        // }
        $produtcs = [
            // 1
            [
                'name' => 'Combi New Diaclasse Auto4cas Elegant สีครีม',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 10,
                'color' => [1],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 2
            [
                'name' => 'Combi New Diaclasse Auto4cas Elegant สีกรม',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 10,
                'color' => [6],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 3
            [
                'name' => 'Combi Mechacal Handy Auto 4 cas สีม่วง',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 12,
                'color' => [8],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 4
            [
                'name' => 'Combi New Diaclasse Auto4cas Elegant สีดำ',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 10,
                'color' => [2],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 5
            [
                'name' => 'Combi New Mechacal Handy สีม่วง-ครีม',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 7,
                'color' => [1, 5],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 6
            [
                'name' => 'Combi Mechacal Handy Auto 4 cas สีกรม',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 12,
                'color' => [6],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 7
            [
                'name' => 'Combi New Diaclasse Auto4cas Elegant แดง',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 10,
                'color' => [5],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 8
            [
                'name' => 'Combi : i-Thruller 4W สีน้ำเงิน',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 17,
                'color' => [6],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 9
            [
                'name' => 'Graco City Cargo สีแดง',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 27,
                'color' => [5],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 10
            [
                'name' => 'Baby Jogger  City Tour 2 – Jet สีดำ',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 26,
                'color' => [2],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 11
            [
                'name' => 'Aprica Optia สีฟ้า',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 6,
                'color' => [7],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 12
            [
                'name' => 'Aprica Luxuna Comfort Auto4 สีน้ำเงิน',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 13,
                'color' => [6],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 13
            [
                'name' => 'Aprica Luxuna Comfort Auto4 สีชมพู',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 13,
                'color' => [8],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 14
            [
                'name' => 'Aprica Luxuna Comfort Auto4 สีม่วง',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 13,
                'color' => [8],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 15
            [
                'name' => 'Aprica Luxuna Comfort Auto4 สีฟ้า',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 13,
                'color' => [7],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 16
            [
                'name' => 'Joie Pact สีเทา',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 5,
                'color' => [3],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 17
            [
                'name' => 'Joie Pact สีแดง',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 5,
                'color' => [5],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],


            // 18
            [
                'name' => 'Aprica Luxuna Comfort Auto4 สีน้ำตาล',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 13,
                'color' => [6],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 19
            [
                'name' => 'Combi New Diaclasse Auto4cas Elegant สีม่วง',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 10,
                'model' => 'New Diaclasse Auto4cas Elegant',
                'color' => [8],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 20
            [
                'name' => 'Combi F2 Plus สีแดง',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 18,
                'color' => [5],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 21
            [
                'name' => 'Recaro Start - X',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 2,
                'color' => [4],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 22
            [
                'name' => 'Aprica Cururila Plus',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 21,
                'color' => [5],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 23
            [
                'name' => 'Aprica Cururila Plus',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 21,
                'color' => [2],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 24
            [
                'name' => 'Combi CULMOVE Smart',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 24,
                'color' => [3],
                'price' => '',
 
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 25
            [
                'name' => 'Recaro Start - X',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 2,
                'color' => [5],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 26
            [
                'name' => 'Ailebebe Kurutto 3i isofix Premium',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 15,
                'color' => [3],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 27
            [
                'name' => 'Combi CULMOVE Smart',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 24,
                'color' => [5],
                'price' => '',
 
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 28
            [
                'name' => 'Combi Culmove Smart 360',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 22,
                'color' => [2],
                'price' => '',
  
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 29
            [
                'name' => 'Combi Culmove Smart 360',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 22,
                'color' => [2],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 30
            [
                'name' => 'Combi CULMOVE Smart',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 24,
                'color' => [4],
                'price' => '',
        
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 31
            [
                'name' => 'Ailebebe Kurutto 3i isofix Premium',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 15,
                'color' => [4],
                'price' => '',
            
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 32
            [
                'name' => 'Combi Culmove Smart (ISO-FIX)',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 23,
                'color' => [5],
                'price' => '',
            
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 33
            [
                'name' => 'Combi Culmove Smart 360',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 22,
                'color' => [4],
                'price' => '',
          
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 34
            [
                'name' => 'Combi Culmove Smart 360',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 22,
                'color' => [2],
                'price' => '',
          
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 35
            [
                'name' => 'Aprica New Fladea Grow Premium',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 9,
                'color' => [5],
                'price' => '',
           
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 36
            [
                'name' => 'Combi Culmove Smart (ISO-FIX)',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 23,
                'color' => [4],
                'price' => '',
           
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 37
            [
                'name' => 'Combi New Luxtia Turn Top Grade',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 8,
                'color' => [2],
                'price' => '',
           
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 38
            [
                'name' => 'Recaro Start - 07',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 3,
                'color' => [6],
                'price' => '',
         
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 39
            [
                'name' => 'Ailebebe Kurutto NT2 Premium',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 14,
                'color' => [1],
                'price' => '',
           
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 40
            [
                'name' => 'Ailebebe Kurutto NT2 Premium',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 14,
                'color' => [5],
                'price' => '',
             
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 41
            [
                'name' => 'Ailebebe Kurutto NT2 Premium',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 14,
                'color' => [2],
                'price' => '',
           
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 42
            [
                'name' => 'Combi New Luxtia Turn Top Grade',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 8,
                'color' => [2],
                'price' => '',
         
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 43
            [
                'name' => 'Combi CULMOVE Smart',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 24,
                'color' => [4],
                'price' => '',
 
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 44
            [
                'name' => 'Combi Culmove Smart 360',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 22,
                'color' => [8],
                'price' => '',
      
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 45
            [
                'name' => 'Ailebebe Kurutto NT2 Premium',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 14,
                'color' => [4],
                'price' => '',
     
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 46
            [
                'name' => 'Baby Jogger City Mini Gt Single',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 31,
                'color' => [2],
                'price' => '',
       
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 47
            [
                'name' => 'Baby Jogger City Mini-Special Edition',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 28,
                'color' => [2],
                'price' => '',
          
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 48
            [
                'name' => 'BABY JOGGER Mini Single',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 29,
                'color' => [5],
                'price' => '',
           
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 49
            [
                'name' => 'Baby Trend  Expediton Elx',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 30,
                'color' => [2],
                'price' => '',
       
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 50
            [
                'name' => 'Air Buggy coco',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 25,
                'color' => [2],
                'price' => '',
          
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 51
            [
                'name' => 'Air Buggy coco',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 25,
                'color' => [5],
                'price' => '',
          
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 52
            [
                'name' => 'Air Buggy coco',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 25,
                'color' => [9],
                'price' => '',
          
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 53
            [
                'name' => 'Air Buggy coco',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 25,
                'color' => [4],
                'price' => '',
    
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 54
            [
                'name' => 'Combi  TWIN SPORT 2 Double Stroller',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 1,
                'color' => [8],
                'price' => '',
             
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 55
            [
                'name' => 'Aprica รุ่น Nelco Bed Twin Thermo',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 11,
                'color' => [9],
                'price' => '',
       
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],


            // 56
            [
                'name' => 'Combi  TWIN SPORT 2 Double Stroller',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 1,
                'color' => [5],
                'price' => '',
            
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 57
            [
                'name' => 'Baby Monsters kuki Twin',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 16,
                'color' => [8],
                'price' => '',
      
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 58
            [
                'name' => 'Baby Monsters kuki Twin',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 16,
                'color' => [6],
                'price' => '',
             
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 59
            [
                'name' => 'Baby Monsters kuki Twin',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 16,
                'color' => [7],
                'price' => '',
         
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 60
            [
                'name' => 'Baby Monsters kuki Twin',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 16,
                'color' => [5],
                'price' => '',
            
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 61
            [
                'name' => 'Joie Evalite Duo',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 19,
                'color' => [2],
                'price' => '',
           
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 62
            [
                'name' => 'Chicco Echo Twin stroller',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 20,
                'color' => [5],
                'price' => '',

                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 63
            [
                'name' => 'Graco  Stadium Duo Stroller',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 4,
                'color' => [2],
                'price' => '',
         
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 64
            [
                'name' => 'Graco  Stadium Duo Stroller',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 4,
                'color' => [2, 4],
                'price' => '',
             
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],


            // 65
            [
                'name' => 'Aprica รุ่น Nelco Bed Twin Thermo',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 11,
                'color' => [9],
                'price' => '',
            
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],



            // ของเล่น

            // 66
            [
                'name' => 'บ้านบอล สไลเดอร์ Haenim house',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 32,
                'color' => [11],
                'price' => '',
            
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 67
            [
                'name' => 'บ้านบอล สไลเดอร์ Haenim house',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 32,
                'color' => [4],
                'price' => '',
          
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 68
            [
                'name' => 'Haenim Dolphin Swing And Slide สไล+ชิงช้า',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 33,
                'color' => [8],
                'price' => '',
       
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 69
            [
                'name' => 'My first kid house by Haenim',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 34,
                'color' => [4],
                'price' => '',
           
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 70
            [
                'name' => 'My first kid house by Haenim',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 34,
                'color' => [11],
                'price' => '',
         
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 71
            [
                'name' => 'Pink Big House With 3 Play Activities',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 35,
                'color' => [8, 10, 11],
                'price' => '',
        
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 72
            [
                'name' => 'ชิงช้า สไลเดอร์ ปีนป่าย Little Castle (Haenim)',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 36,
                'color' => [6],
                'price' => '',
           
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 73
            [
                'name' => 'สไลเดอร์ บ้าน House Slide',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 37,
                'color' => [4],
                'price' => '',
             
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 74
            [
                'name' => 'บ้านน้อย 2 ชั้น',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 38,
                'color' => [7, 8, 11],
                'price' => '',
              
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 75
            [
                'name' => 'บ้านของเล่น',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 39,
                'color' => [],
                'price' => '',
            
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 76
            [
                'name' => 'บ้านขนมปังหนูน้อย',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 40,
                'color' => [4],
                'price' => '',
           
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 77
            [
                'name' => 'บ้านไม้กลางป่า',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 41,
                'color' => [4],
                'price' => '',
              
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 78
            [
                'name' => 'บ้านกระดาน',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 42,
                'color' => [9, 11],
                'price' => '',
            
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 79
            [
                'name' => 'บ้านต้นไม้',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 43,
                'color' => [4, 10],
                'price' => '',
       
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 80
            [
                'name' => 'กระดานลื่นเดี่ยวซ้อนเก็บได้',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 44,
                'color' => [10],
                'price' => '',
         
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 81
            [
                'name' => 'กระดานลื่นดอกไม้',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 45,
                'color' => [4],
                'price' => '',
             
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 82
            [
                'name' => 'กระดานลื่นแพนด้าน้อย',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 46,
                'color' => [11],
                'price' => '',
              
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 83
            [
                'name' => 'บ้านกระดานลื่น 2 ชั้น',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 47,
                'color' => [11],
                'price' => '',
         
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 84
            [
                'name' => 'กระดานลื่นผลไม้รวม',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 48,
                'color' => [9],
                'price' => '',
            
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],

            // 85
            [
                'name' => 'กระดานลื่นกระต่ายพาชู๊ต',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 49, 
                'color' => [9],
                'price' => '',
             
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],


            // 86
            [
                'name' => 'ชิงช้ายีราฟ',
                'deposit' => 500,
                'youtube_url' => '',
                'product_model_id' => 50,
                'color' => [9],
                'price' => '',
            
                'marks' => [
                    [
                        'des' => '',
                        'image_url' => ''
                    ],
                ],
            ],
        ];


        foreach ($produtcs as $product) {
            $product = (object) $product;
            $products = Product::create([
                "name" => $product->name,
                // "description" => $product->description,
                // "from_age" => $product->from_age,
                // "to_age" => $product->to_age,
                "deposit" => $product->deposit,
                "youtube_url" => $product->youtube_url,
                "product_model_id" => $product->product_model_id,
                // "model" => $product->model,
                // "weight" => $product->weight,


            ]);

            if (!empty($product->color)) {
                $products->update(
                    [
                        "color" => $product->color
                    ]
                );
            }


            foreach ($product->marks as $mark) {
                $mark = (object) $mark;
                if ($mark->des && $mark->image_url) {
                    $mark = ProductMark::create([
                        "des" => $mark->des,
                        "image_url" => $mark->image_url
                    ]);
                }
            }
        }
    }
}