<?php

use Illuminate\Database\Seeder;
use App\Address;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Address::insert([
            [
                'information' => '427',
                'subdistrict' => 'บางขุนเทียน',
                'district' => 'จอมทอง',
                'province' => 'กรุงเทพมหานคร',
                'zipcode' => '10150',
                'mobile_number' => '0900000000',
                'user_id' => 1
            ],
            [
                'information' => '552',
                'subdistrict' => 'บางพรม',
                'district' => 'ตลิ่งชัน',
                'province' => 'กรุงเทพมหานคร',
                'zipcode' => '10170',
                'mobile_number' => '0800000001',
                'user_id' => 1
            ],
            [
                'information' => '432',
                'subdistrict' => 'บางพรม',
                'district' => 'ตลิ่งชัน',
                'province' => 'กรุงเทพมหานคร',
                'zipcode' => '10170',
                'mobile_number' => '0800000001',
                'user_id' => 3
            ],
        ]);
    }
}