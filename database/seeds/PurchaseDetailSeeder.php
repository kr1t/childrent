<?php

use Illuminate\Database\Seeder;
use App\PurchaseDetail;

class PurchaseDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PurchaseDetail::insert([
            [
                'purchase_id' => 1,
                'product_id' => 1,
                // 'amount' => 10,
                // 'product_amount_id' => 1,
                // 'product_price_id' => 1,
                'price_all' => 4200,
                'created_at' => '2020-03-08 00:00:00.000000',
                'pickup_date' => '2020-03-11 00:00:00.000000',
                'return_date' => '2020-03-18 00:00:00.000000'
                
            ],
            [
                'purchase_id' => 2,
                'product_id' => 2,
                // 'amount' => 10,
                // 'product_amount_id' => 2,
                // 'product_price_id' => 1,
                'price_all' => 4800,
                'created_at' => '2020-03-09 00:00:00.000000',
                'pickup_date' => '2020-03-11 00:00:00.000000',
                'return_date' => '2020-03-18 00:00:00.000000'
            ],
            [
                'purchase_id' => 3,
                'product_id' => 3,
                // 'amount' => 10,
                // 'product_amount_id' => 2,
                // 'product_price_id' => 1,
                'price_all' => 2900,
                'created_at' => '2020-03-10 00:00:00.000000',
                'pickup_date' => '2020-03-12 00:00:00.000000',
                'return_date' => '2020-03-19 00:00:00.000000'
            ],
            [
                'purchase_id' => 4,
                'product_id' => 4,
                // 'amount' => 10,
                // 'product_amount_id' => 2,
                // 'product_price_id' => 1,
                'price_all' => 3400,
                'created_at' => '2020-03-11 00:00:00.000000',
                'pickup_date' => '2020-03-13 00:00:00.000000',
                'return_date' => '2020-03-20 00:00:00.000000'
            ],
            [
                'purchase_id' => 5,
                'product_id' => 5,
                // 'amount' => 10,
                // 'product_amount_id' => 2,
                // 'product_price_id' => 1,
                'price_all' => 7200,
                'created_at' => '2020-03-12 00:00:00.000000',
                'pickup_date' => '2020-03-15 00:00:00.000000',
                'return_date' => '2020-03-22 00:00:00.000000'
                
            ],
            [
                'purchase_id' => 6,
                'product_id' => 6,
                // 'amount' => 10,
                // 'product_amount_id' => 2,
                // 'product_price_id' => 1,
                'price_all' => 3600,
                'created_at' => '2020-03-13 00:00:00.000000',
                'pickup_date' => '2020-03-15 00:00:00.000000',
                'return_date' => '2020-03-22 00:00:00.000000'
            ],
        ]);
    }
}