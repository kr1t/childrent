<?php

use Illuminate\Database\Seeder;
use App\CategoryType;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // หมวดหมู่
        Category::insert([

            // รถเข็นเด็ก
            // 1
            [
                'name' => 'Full size Stroller',
                'description' => '',
                'img_url' => '/images/category/full.jpg',
                'slug' => 'Full-size-Stroller',
                'group' => 'Stroller'
            ],
            // 2
            [
                'name' => 'Jogging Stroller',
                'description' => '',
                'img_url' => '/images/category/jogging.jpg',
                'slug' => 'Jogging-Stroller',
                'group' => 'Stroller'
            ],
            // 3
            [
                'name' => 'Double Stroller',
                'description' => '',
                'img_url' => '/images/category/double.jpg',
                'slug' => 'Double-Stroller',
                'group' => 'Stroller'
            ],
            // 4
            [
                'name' => 'Umbrella Stroller',
                'description' => '',
                'img_url' => '/images/category/umbrella.jpg',
                'slug' => 'Umbrella-Stroller',
                'group' => 'Stroller'
            ],
            // 5
            [
                'name' => 'Sit ‘n Stand Stroller',
                'description' => '',
                'img_url' => '/images/category/sitnstand.jpg',
                'slug' => 'Sit-n-Stand-Stoller',
                'group' => 'Stroller'
            ],
            // 6
            [
                'name' => 'Carseat Stroller',
                'description' => '',
                'img_url' => '/images/category/3in1.jpg',
                'slug' => 'Carseat-Stroller',
                'group' => 'Stroller'
            ],



            // -------------------------------------
            // คาร์ซีท
            //7
            [
                'name' => 'Rear-Facing',
                'description' => '- สำหรับเด็กอายุ 0-2 ปี <br>
                          - คาร์ซีทแบบนั่งหันหน้าเข้าเบาะ และยังสามารถปรับเอนไปกับที่นั่งได้ <br>
                          - เหมาะสำหรับเด็กที่มีน้ำหนักตัวไม่เกิน 10 กก. <br>
                          - สามารถปกป้องหัวของเด็ก ลำคอ และกระดูกสันหลังได้ดี',
                'img_url' => '/images/category/rear.jpg',
                'slug' => 'Rear-Facing',
                'group' => 'Carseat'
            ],
            //8
            [
                'name' => 'Forward-Facing',
                'description' => '- สำหรับเด็กอายุ 2-7 ปี <br>
                          - คาร์ซีทแบบที่นั่งทิศทางเดียวกับเบาะ <br>
                          - เหมาะกับเด็กที่มีน้ำหนักตัวเกิน 9 กก.',
                'img_url' => '/images/category/forward.jpg',
                'slug' => 'Forward-Facing',
                'group' => 'Carseat'
            ],
            //9
            [
                'name' => 'Booster',
                'description' => '- สำหรับเด็กอายุ 4-12 ปี <br>
                          - คาร์ซีทแบบเบาะนั่งเสริมความสูง พนักพิงด้านหลัง <br>
                          - เหมาะกับเด็กที่มีน้ำหนักตัว 15- 18  กก.',
                'img_url' => '/images/category/booster.jpg',
                'slug' => 'Booster',
                'group' => 'Carseat'
            ],



            // -------------------------------------
            // ของเล่น
            //10

            [
                'name' => 'ของเล่นเสริมพัฒนาการ',
                'description' => '',
                'img_url' => '',
                'slug' => 'Early-development-toys',
                'group' => 'Toy'
            ],

            //11
            [
                'name' => 'ของเล่นเสริมบุคลิกภาพ',
                'description' => '',
                'img_url' => '',
                'slug' => 'Personality-supplement-toys',
                'group' => 'Toy'
            ],

            //12
            [
                'name' => 'ของเล่นเสริมทักษะ',
                'description' => '',
                'img_url' => '',
                'slug' => 'Skill-toys',
                'group' => 'Toy'
            ],

            //13
            [
                'name' => 'ของเล่นเสริมบทบาทสมมติ',
                'description' => '',
                'img_url' => '',
                'slug' => 'Role-play-toys',
                'group' => 'Toy'
            ],

            //14
            [
                'name' => 'ของเล่นเสริมกล้ามเนื้อ',
                'description' => '',
                'img_url' => '',
                'slug' => 'Muscular-Toys',
                'group' => 'Toy'
            ],



        ]);
    }
}