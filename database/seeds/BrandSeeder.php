<?php

use Illuminate\Database\Seeder;
use App\Brand;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Brand::insert([
            // 1
            [
                'name' => 'No Brand',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'no-brand'
            ],
            // 2
            [
                'name' => 'Combi',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'combi'
            ],
            // 3
            [
                'name' => 'Graco',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'graco'
            ],
            // 4
            [
                'name' => 'Baby Jogger',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'baby-jogger'
            ],
            // 5
            [
                'name' => 'Aprica',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'aprica'
            ],
            // 6
            [
                'name' => 'Joie',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'joie'
            ],
            // 7
            [
                'name' => 'Recaro',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'Recaro'
            ],
            // 8
            [
                'name' => 'Ailebebe',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'Ailebebe'
            ],
            // 9
            [
                'name' => 'Baby Trend',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'Baby-Trend'
            ],
            // 10
            [
                'name' => 'Air Buggy',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'Ai- Buggy'
            ],
            // 11
            [
                'name' => 'Baby Monsters',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'Baby-Monsters '
            ],
            // 12
            [
                'name' => 'Chicco',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'Chicco'
            ],

            // 13
            [
                'name' => 'Haenim',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'Haenim'
            ],
            // 14
            [
                'name' => 'Huangdo',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'Huangdo'
            ],
            // 15
            [
                'name' => 'LERADO',
                'description' => ' ',
                'img_url' => ' ',
                'slug' => 'LERADO'
            ],


        ]);
    }
}