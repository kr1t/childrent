<?php

use Illuminate\Database\Seeder;
use App\Color;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Color::insert([
            //1
            [
                'name' => 'ครีม',
                'code' => '#ffffcc'
            ],

            //2
            [
                'name' => 'ดำ',
                'code' => '#1a1a1a'
            ],

            //3
            [
                'name' => 'เทา',
                'code' => '#d9d9d9'
            ],

            //4
            [
                'name' => 'น้ำตาล',
                'code' => '#996600'
            ],

            //5
            [
                'name' => 'แดง',
                'code' => '#ff5050'
            ],

            //6 น้ำเงิน กรม
            [
                'name' => 'น้ำเงิน',
                'code' => '#333399'
            ],

            //7
            [
                'name' => 'ฟ้า',
                'code' => '#00ccff'
            ],

            //8  ชมพู  ม่วง
            [
                'name' => 'ม่วง',
                'code' => '#ff66ff'
            ],

            //9
            [
                'name' => 'เหลือง',
                'code' => '#ffff1a'
            ],

            //10
            [
                'name' => 'เขียว',
                'code' => '#66ff66'
            ],

            //11
            [
                'name' => 'ขาว',
                'code' => '#ffffff'
            ],

        ]);
    }
}