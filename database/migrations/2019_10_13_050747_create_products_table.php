<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');

            $table->integer('deposit');
            $table->string('youtube_url')->nullable();
            // $table->string('model');
            $table->text('color')->comment('json')->nullable();
            // $table->text('marks')->comment('json');
            // $table->integer('discount')->nullable();
            $table->integer('price')->nullable();
            $table->integer('status')->comment('1.ปกติ 2.กำลังถูกเช่า 3.กำลังซ่อม 4.กำลังทำความสะอาด')->default(1);

            // $table->bigInteger('category_id')->unsigned()->default(1);
            // $table->foreign('category_id', 'pd_category_id_foreign')
            //     ->references('id')
            //     ->on('categories')
            //     ->onDelete('cascade');

            // $table->bigInteger('brand_id')->unsigned()->default(1);
            // $table->foreign('brand_id')
            //     ->references('id')
            //     ->on('brands')
            //     ->onDelete('cascade');
            $table->timestamp('delete_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}