import axios from "axios";
import * as types from "../mutation-types";

// state
export const state = {
  brands: null
};

// getters
export const getters = {
  brands: state => state.brands
};

// mutations
export const mutations = {
  [types.FETCH_BRAND](state, { data }) {
    state.brands = data;
  }
};

// actions
export const actions = {
  async fetch({ commit }) {
    try {
      const { data } = await axios.get("/api/brands");

      commit(types.FETCH_BRAND, { data });
    } catch (e) {
      console.log(e);
    }
  }
};
