import axios from "axios";
import * as types from "../mutation-types";

// state
export const state = {
  items: null
};

// getters
export const getters = {
  items: state => state.items
};

// mutations
export const mutations = {
  [types.FETCH_ADPC](state, { data }) {
    state.items = data;
  }
};

// actions
export const actions = {
  async fetch({ commit }, { q = "", status = 1 }) {
    try {
      const { data } = await axios.get(
        "/api/admin/purchase?q=" + q + "&status=" + status
      );

      commit(types.FETCH_ADPC, { data });
    } catch (e) {
      console.log(e);
    }
  }
};
