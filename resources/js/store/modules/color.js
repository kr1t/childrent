import axios from "axios";
import * as types from "../mutation-types";

// state
export const state = {
  colors: null
};

// getters
export const getters = {
  colors: state => state.colors
};

// mutations
export const mutations = {
  [types.FETCH_COLOR](state, { data }) {
    state.colors = data;
  }
};

// actions
export const actions = {
  async fetch({ commit }) {
    try {
      const { data } = await axios.get("/api/colors");

      commit(types.FETCH_COLOR, { data });
    } catch (e) {
      console.log(e);
    }
  }
};
