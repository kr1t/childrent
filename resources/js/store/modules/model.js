import axios from "axios";
import * as types from "../mutation-types";

// state
export const state = {
  models: null
};

// getters
export const getters = {
  models: state => state.models
};

// mutations
export const mutations = {
  [types.FETCH_MODEL](state, { data }) {
    state.models = data;
  }
};

// actions
export const actions = {
  async fetch({ commit }) {
    try {
      const { data } = await axios.get("/api/models");

      commit(types.FETCH_MODEL, { data });
    } catch (e) {
      console.log(e);
    }
  }
};
