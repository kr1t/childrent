import axios from "axios";
import * as types from "../mutation-types";

// state
export const state = {
  news: null,
  mininews: null,
  show: null
};

// getters
export const getters = {
  news: state => state.news,
  mininews: state => state.mininews,
  show: state => state.show
};

// mutations
export const mutations = {
  [types.FETCH_NEWS](state, news) {
    state.news = news;
  },
  [types.FETCH_MININEWS](state, data) {
    state.mininews = data;
  },
  [types.FETCH_NEWS_SHOW](state, show) {
    state.show = show;
  }
};

// let url = '/api/comment/'
// actions
export const actions = {
  async fetch({ commit }) {
    try {
      const { data } = await axios.get(`/api/news`);
      commit(types.FETCH_NEWS, data);
    } catch (e) {}
  },
  async fetchmini({ commit }) {
    try {
      const { data } = await axios.get(`/api/mini_news`);
      commit(types.FETCH_MININEWS, data);
    } catch (e) {}
  },
  async show({ commit }, id) {
    try {
      const { data } = await axios.get(`/api/news/${id}`);
      commit(types.FETCH_NEWS_SHOW, data);
    } catch (e) {}
  }
};
