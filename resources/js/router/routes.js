function page(path) {
  return () =>
    import(/* webpackChunkName: '' */ `~/pages/${path}`).then(
      m => m.default || m
    );
}

const News = () => import("~/pages/news/index").then(m => m.default || m);
const Newshow = () => import("~/pages/news/show").then(m => m.default || m);
const AdminAddnew = () =>
  import("~/pages/admin/addnew").then(m => m.default || m);

const AdminArticle = () =>
  import("~/pages/admin/article").then(m => m.default || m);

export default [
  { path: "/", name: "welcome", component: page("welcome.vue") },

  { path: "/test", name: "test", component: page("1test/products.vue") },

  { path: "/omise", name: "omise", component: page("paymentOmise.vue") },

  { path: "/login", name: "login", component: page("auth/login.vue") },
  { path: "/register", name: "register", component: page("auth/register.vue") },
  { path: "/article", name: "news", component: News },
  { path: "/article/:id", name: "newshow", component: Newshow },
  {
    path: "/password/reset",
    name: "password.request",
    component: page("auth/password/email.vue")
  },
  {
    path: "/password/reset/:token",
    name: "password.reset",
    component: page("auth/password/reset.vue")
  },
  {
    path: "/email/verify/:id",
    name: "verification.verify",
    component: page("auth/verification/verify.vue")
  },
  {
    path: "/bills/:id",
    name: "bill.show",
    component: page("bill.vue")
  },
  {
    path: "/email/resend",
    name: "verification.resend",
    component: page("auth/verification/resend.vue")
  },

  { path: "/home", name: "home", component: page("home.vue") },
  {
    path: "/settings",
    component: page("settings/index.vue"),
    children: [
      { path: "", redirect: { name: "settings.profile" } },
      {
        path: "profile",
        name: "settings.profile",
        component: page("settings/profile.vue")
      },
      {
        path: "address",
        name: "settings.address",
        component: page("settings/address.vue")
      },
      {
        path: "address/create",
        name: "settings.address.create",
        component: page("settings/address-create.vue")
      },
      {
        path: "address/:id/edit",
        name: "settings.address.edit",
        component: page("settings/address-create.vue")
      },

      {
        path: "password",
        name: "settings.password",
        component: page("settings/password.vue")
      }
    ]
  },
  {
    path: "/admin",
    component: page("admin/index.vue"),
    children: [
      { path: "", redirect: { name: "admin.stats" } },
      { path: "article/create", name: "admin.adnews", component: AdminAddnew },
      {
        path: "article/:id/edit",
        name: "admin.adnews",
        component: AdminAddnew
      },
      { path: "article", name: "admin.article", component: AdminArticle },
      {
        path: "dashboard",
        name: "admin.dashboard",
        component: page("admin/dashboard.vue")
      },
      {
        path: "products",
        name: "admin.products",
        component: page("admin/product/ad-product.vue")
      },
      {
        path: "products/create",
        name: "admin.products.form",
        component: page("admin/product/form.vue")
      },
      {
        path: "products/:id/edit",
        name: "admin.products.form",
        component: page("admin/product/form.vue")
      },
      {
        path: "categories",
        name: "admin.categories",
        component: page("admin/category/index.vue")
      },
      {
        path: "categories/create",
        name: "admin.categories.form",
        component: page("admin/category/form.vue")
      },
      {
        path: "categories/:id/edit",
        name: "admin.categories.form",
        component: page("admin/category/form.vue")
      },
      {
        path: "colors",
        name: "admin.colors",
        component: page("admin/color/index.vue")
      },
      {
        path: "colors/create",
        name: "admin.colors.form",
        component: page("admin/color/form.vue")
      },
      {
        path: "colors/:id/edit",
        name: "admin.colors.form",
        component: page("admin/color/form.vue")
      },
      {
        path: "models",
        name: "admin.models",
        component: page("admin/models/index.vue")
      },
      {
        path: "models/create",
        name: "admin.models.form",
        component: page("admin/models/form.vue")
      },
      {
        path: "models/:id/edit",
        name: "admin.models.form",
        component: page("admin/models/form.vue")
      },
      {
        path: "brands",
        name: "admin.brands",
        component: page("admin/brand/index.vue")
      },
      {
        path: "brands/create",
        name: "admin.brands.form",
        component: page("admin/brand/form.vue")
      },
      {
        path: "brands/:id/edit",
        name: "admin.brands.form",
        component: page("admin/brand/form.vue")
      },
      {
        path: "brands/create",
        name: "admin.brands.form",
        component: page("admin/brand/form.vue")
      },
      {
        path: "orderstatus/:status",
        name: "admin.orderstatus",
        component: page("admin/orderstatus/index.vue")
      },
      {
        path: "stats",
        name: "admin.stats",
        component: page("admin/stats.vue")
      }
    ]
  },
  {
    path: "/products",
    name: "product.all",
    component: page("product/index.vue")
  },
  {
    path: "/mycarts",
    name: "cart",
    component: page("cart/index.vue")
  },
  {
    path: "/mypurchase",
    name: "purchase",
    component: page("myPurchase/index.vue")
  },
  {
    path: "/mycarts/checkout",
    name: "cart.checkout",
    component: page("cart/payment.vue")
  },
  {
    path: "/swap/purchase/:purchase_id/checkout/:product_id",
    name: "swap.checkout",
    component: page("cart/payment.vue")
  },
  {
    path: "/mypackage",
    name: "my.package",
    component: page("myPackage/index.vue")
  },
  {
    path: "/products/:id",
    name: "product.show",
    component: page("product/show.vue")
  },
  {
    path: "/howto",
    name: "howto",
    component: page("howto/index.vue")
  },
  {
    path: "/about",
    name: "about",
    component: page("about/index.vue")
  },
  {
    path: "/contact",
    name: "contact",
    component: page("contact/index.vue")
  },
  {
    path: "/category/:slug",
    name: "category.show",
    component: page("category/show.vue")
  },
  {
    path: "/payment/:payment_by",
    name: "payment",
    component: page("payment/Index.vue")
  },

  { path: "*", component: page("errors/404.vue") }
];
