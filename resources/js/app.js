import Vue from "vue";
import store from "~/store";
import router from "~/router";
import i18n from "~/plugins/i18n";
import App from "~/components/App";
import "~/plugins";
import "~/components";
import BootstrapVue from "bootstrap-vue";
import VueThailandAddress from "vue-thailand-address";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
Vue.use(ElementUI);

Vue.use(BootstrapVue);
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import Mixins from "./mixins";
import VueCarousel from "vue-carousel";
Vue.use(VueCarousel);
import VeLine from "v-charts/lib/line.common";
import VeMap from "v-charts/lib/map.common";
// 下面的依赖可以按照需求选择性加载
// The following dependencies can be selectively loaded on demand
import "echarts/lib/component/markLine";
import "echarts/lib/component/markPoint";
import "echarts/lib/component/markArea";
import "echarts/lib/component/visualMap";
import "echarts/lib/component/dataZoom";
import "echarts/lib/component/toolbox";
import "echarts/lib/component/title";
import "zrender/lib/svg/svg";
import "v-charts/lib/style.css";

[VeLine, VeMap].forEach(comp => {
  Vue.component(comp.name, comp);
});

Vue.config.productionTip = false;

// เพิ่ม stylesheet ของ Vue Thailand Address เข้าไป
import "vue-thailand-address/dist/vue-thailand-address.css";
import VueCtkDateTimePicker from "vue-ctk-date-time-picker";
import "vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css";
import TrendChart from "vue-trend-chart";

import VueFusionCharts from "vue-fusioncharts";
import FusionCharts from "fusioncharts";
import TimeSeries from "fusioncharts/fusioncharts.timeseries";
Vue.use(VueFusionCharts, FusionCharts, TimeSeries);
import VueApexCharts from "vue-apexcharts";
Vue.use(VueApexCharts);

Vue.component("apexchart", VueApexCharts);
Vue.use(TrendChart);
Vue.component("VueCtkDateTimePicker", VueCtkDateTimePicker);
// ใช้ Plugin
Vue.use(VueThailandAddress);
Vue.mixin(Mixins);

/* eslint-disable no-new */
new Vue({
  i18n,
  store,
  router,
  ...App
});
