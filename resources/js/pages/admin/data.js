export const LINE_DATA = {
  columns: ["วันที่", "เงิน"],
  rows: [
    { วันที่: "1/1", เงิน: 1391 },
    { วันที่: "1/2", เงิน: 3530 },
    { วันที่: "1/3", เงิน: 2923 },
    { วันที่: "1/4", เงิน: 1723 },
    { วันที่: "1/5", เงิน: 3792 },
    { วันที่: "1/6", เงิน: 4593 }
  ]
};
