[
'title'='ความสำคัญของ "คาร์ซีท" จำเป็นต่อชีวิตความปลอดภัยลูกมากที่สุด',
'img'='/images/article/1.jpg',
'content'='<p><span style="color: rgb(126, 126, 126);">อย่าคิดว่า</span><strong style="color: rgb(126, 126, 126);">คาร์ซีท</strong><span style="color: rgb(126, 126, 126);">คือของฟุ่มเฟือยเกินจำเป็น เพราะนี่คือตัวช่วยสำคัญรักษาชีวิต ป้องกัน</span><strong style="color: rgb(126, 126, 126);">อุบัติเหตุ</strong><span style="color: rgb(126, 126, 126);">ที่ไม่คาดฝันให้ลูกรักขณะอยู่ระหว่าง</span><strong style="color: rgb(126, 126, 126);">การเดินทาง</strong><span style="color: rgb(126, 126, 126);">ในรถยนต์ได้</span></p>
<p class="ql-align-center"></p>
<p><span style="color: rgb(126, 126, 126);">สำหรับคุณพ่อคุณแม่ที่เวลาพาลูกเล็กๆ เดินทางไปไหนด้วย ก็มักจะเอาเขามานั่งบนตักของคุณระหว่างขับรถ รู้รึเปล่าว่าการทำแบบนั้นเป็นอันตรายต่อลูกถึงชีวิต เพราะหากมีอุบัติเหตุไม่คาดฝันเกิดขึ้น ลูกจะกลายเป็นถุงลมนิรภัยชั้นดีให้คุณแทน ลูกจะเป็นตัวดูดซับแรงกระแทกจากถุงลมนิรภัยให้คุณอีกที และอาจโดนอัดอยู่ระหว่างคุณ กับถุงลมนิรภัย จนขาดอากาศหายใจ หรืออาจกระแทกเข้ากับคอนโซลหน้ารถ หากมีการเบรคแรงๆ อย่างกระทันหัน</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">ที่นั่งที่ปลอดภัยที่สุดสำหรับเด็ก คือ เบาะที่นั่งด้านหลัง แต่ถึงกระนั้นวิธีการเอาเด็กนั่งรถยนต์ที่ถูกต้องที่สุด และปลอดภัยกับเด็กที่สุด จะต้องให้เด็กนั่งอยู่บนคาร์ซีต (Car Seat) โดยเอาคาร์ซีตวางไว้บนเบาะหลังอีกที</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">คาร์ซีตจึงไม่ใช่ของเกินความจำเป็น หรือเหมาะกับคนมีเงินเท่านั้น พ่อแม่ที่มีรถขับต้องถือว่าความปลอดภัยของลูกมีความสำคัญสูงสุด อย่าชะล่าใจ เพราะอุบัติเหตุเกิดขึ้นได้เสมอ โดยไม่จำเป็นต้องให้กฎหมายออกมาบังคับใช้ เหมือนที่ต่างประเทศถึงจะมาค่อยให้ความสำคัญกัน</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">หากสงสัยว่าแค่คาร์ซีต หรือเบาะนั่งสำหรับเด็กในรถยนต์ จะช่วยในเรื่องความปลอดภัยได้อย่างไร ให้ลองนึกภาพเสี้ยววินาทีที่เกิดอุบัติเหตุดู ว่าตอนนั้น คุณพ่อคุณแม่ไม่มีทางจะจับยึดตัวลูกไว้ได้แน่ๆ จึงมีสิทธิ์ที่ลูกจะหลุดกระเด็นออกนอกตัวรถ หรือกระแทกเข้ากับส่วนต่างๆ ของรถได้ แต่คาร์ซีทที่ได้มาตรฐาน และใช้งานอย่างถูกวิธี จะช่วยป้องกันกรณีแบบนี้ได้</span></p>
<p><br></p>
<p><br></p>
<p><strong style="color: rgb(126, 126, 126);">วิธีเลือกซื้อคาร์ซีตที่ได้มาตรฐาน</strong></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">1. ควรเลือกคาร์ซีตที่มีขนาดพอดีกับน้ำหนักตัว และส่วนสูงของลูก เพื่อให้ลูกนั่งสบาย และเพื่อที่สายรัดจะได้อยู่ในตำแหน่งที่ถูกต้องไม่รัดคอลูกให้อึดอัด หรือถูกดึงรั้งตอนเกิดอุบัติเหตุ และไม่หลวมเกินไป จนเด็กหลุดออกจากคาร์ซีทไปกับกระแทกกับส่วนต่างๆของรถ หรือหลุดออกนอกตัวรถ</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">2. คาร์ซีตมาตรฐานมีอยู่ 3 แบบ คือ</span></p>
<p><span style="color: rgb(126, 126, 126);">- Rear-Facing Infant Seats and Convertible Seats คือ คาร์ซีทแบบนั่งหันหน้าไปด้านหลังรถ และแบบปรับเอนไปกับที่นั่ง เหมาะสำหรับเด็กแรกเกิดจนถึงอายุประมาณ 12 เดือน และเด็กที่น้ำหนักตัวไม่เกิน 10 กก. คาร์ซีทชนิดนี้จะปกป้องหัวของเด็กลำคอ และกระดูกสันหลังได้ดีที่สุด</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">- Forward-facing child seats คือ คาร์ซีตแบบที่นั่งหันไปทางหน้ารถ เหมาะกับเด็กที่มีอายุมากกว่า 1 ขวบและมีน้ำหนักตัวเกิน 9 กก.</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">- Booster seats คือ คาร์ซีตแบบมีพนักพิงด้านหลัง ซึ่งเหมาะกับเด็กที่มีน้ำหนักตัว 15- 18 กิโลกรัม และ Booster seat แบบไม่มีพนักพิงด้านหลัง ซึ่งจะเหมาะกับเด็กที่มีน้ำหนักตัว 22 -25 ก.ก. และสามารถนั่งตัวตรงได้แล้ว ซึ่งก็คือเด็กที่มีอายุตั้งแต่ 6 ปีขึ้นไป</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">3. เมื่อดูที่ประเภทของคาร์ซีตกันแล้ว ก็ต้องดูที่ประเภทของสายรัดกันด้วย เพราะสายรัดที่จะรัดตัวเด็กให้ติดกับคาร์ซีตนั้น มีหลายแบบตามความเหมาะสม ซึ่งเราต้องเลือกให้ถูกด้วย โดยสายรัดจะแบ่งเป็นแบบต่างๆ ดังนี้</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">- สายรัดแบบรัด 3 จุด จะมีสายรัด 3 เส้น ให้รัดตรงบ่า 2 ทั้งสองข้างของเด็ก แล้วยาวลงมาเชื่อมล็อคใกล้ๆ ด้านล่างของที่นั่ง</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">- สายรัดแบบ 5 จุด จะมีสายรัด 5 เส้น แบ่งเป็น 2 เส้นที่บ่า อีก 2 เส้นที่สะโพก และอีกเส้นที่ เป้ากางเกง ซึ่งเหมาะกับเด็กทารกที่มีน้ำหนักไม่เกิน 9 ก.ก.</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">- แบบ Overhead shield จะมีคานขนาดใหญ่พอสมควร ติดอยู่ด้านหน้าของเบาะ เพื่อกันไม่ให้เด็กกระเด็นหลุดออกจากคาร์ซีต แล้วไปกระแทก หรือเหวี่ยงไปโดนส่วนต่างๆ ของรถ</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">- แบบ T- Shield จะมีเป็นคานรูปสามเหลี่ยมอยู่ติดกับสายรัดช่วงบ่า</span></p>
<p><br></p>
<p><br></p>
<p><strong style="color: rgb(126, 126, 126);">วิธีการใช้คาร์ซีตให้ถูกวิธี</strong></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">1. เมื่อนำเด็กทารกเกิดใหม่มานั่งคาร์ซีต ควรปรับมุมการนั่งของเด็กให้ได้ 45 องศา ซึ่งคาร์ซีตบางรุ่นก็มีขีดแสดงองศาให้ดูง่ายขึ้น แต่ถ้าไม่มีก็ต้องไปหากระดาษสี่</span></p>
<p><span style="color: rgb(126, 126, 126);">เหลี่ยมจัตุรัสมาใบหนึ่ง แล้วพับครึ่งตามมุม มุมนั่นแหละคือ มุม 45 องศาที่เราจะใช้เป็นตัวเปรียบเทียบ ด้วยการเอาด้านที่ยาวที่สุดของสามเหลี่ยมไปลอง</span></p>
<p><span style="color: rgb(126, 126, 126);">ทาบกับแนวพนักพิงหลังของคาร์ซีต ถ้าเราทาบทำมุมถูกต้อง และแน่นหนาดีพอ ด้านสั้นของสามเหลี่ยมจะต้องทำมุมขนานกับพื้นโลก และนั่นหมายถึงเราติดตั้งคาร์ซีททำมุม 45 องศาได้ตามต้องการแล้ว</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">2. เมื่อติดตั้งคาร์ซีตลงบนเบาะของรถ จะต้องตั้งไว้ใกล้กับจุดที่เข็มขัดนิรภัยของรถผ่านลอดลงมาที่ตัวคาร์ซีท แล้วใช้เข็มขัดนิรภัยล็อคคาร์ซีทให้แน่นๆ ถ้าคาร์ซีทนั้นติดตั้งอย่างถูกต้อง ทีนี้จะขยับมันไปทางซ้าย ทางขวา หรือข้างหน้าได้ไม่เกิน 1 นิ้ว ถ้าขยับคาร์ซีตได้มากกว่านี้แสดงว่าติดตั้งไม่แน่นพอ</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">3. สายรัดจะต้องไม่สามารถดึงขึ้นมาเป็นจีบๆ ได้ จะต้องตึงราบไปกับลูก ถ้ายังดึงสายรัดขึ้นมาได้ แปลว่ายังรัดไม่แน่นพอ</span></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">4. เด็กทารกที่อายุยังไม่ถึง 1 ขวบ และน้ำหนักตัวยังไม่ถึง 9 ก.ก. ยังไม่ควรให้นั่งคาร์ซีตแบบหันไปด้านหน้ารถ เพราะกระดูกสันหลังของเด็กวัยนี้ยังไม่สมบูรณ์ เมื่อเกิดการเบรคแรงๆ หรือเกิดอุบัติเหตุ ศีรษะของเด็กจะถูกแรงเหวี่ยงของตัวรถ กระชากไปข้างหน้า ทำให้เสียชีวิต หรือ เป็นอัมพาตได้</span></p>
<p><br></p>
<p><br></p>
<p><strong style="color: rgb(126, 126, 126);">เมื่อไหร่ที่ควรเลิกใช้คาร์ซีต</strong></p>
<p><br></p>
<p><span style="color: rgb(126, 126, 126);">1. เมื่อลูกสูงพอที่จะนั่งห้อยขา แล้วขายาวถึงพื้นพอดี</span></p>
<p><span style="color: rgb(126, 126, 126);">2. เมื่อลูกโตพอที่จะนั่งตัวตรงได้แล้ว</span></p>
<p><span style="color: rgb(126, 126, 126);">3. เข็มขัดนิรภัยสามารถรัดตรงส่วนกระดูกเชิงกรานของลูกได้พอดี ไม่ใช่ไปรัดอยู่ตรงหน้าท้อง</span></p>
<p><span style="color: rgb(126, 126, 126);">5. เมื่อคาดเข็มขัดนิรภัยที่ส่วนบ่า จะต้องพาดมาตรงส่วนหน้าอก ไม่ใช่ผ่านมาตรงแขน หรือคอของลูก</span></p>
<p><span style="color: rgb(126, 126, 126);">6. เมื่อลูกอายุ 8 ขวบ หรือสูงเกิน 150 ซ.ม. และสามารถรัดเข็มขัดนิรภัยได้ถูกต้องแล้ว</span></p>'
],
[
'title'='วิจัยพบ เด็กหัดเดินเข้าใจการนับก่อนเรียนรู้เรื่องตัวเลข',
'img'='/images/article/2.jpg',
'content'='<h2><br></h2>
<h2><span style="color: rgb(28, 30, 33);">คุณพ่อคุณแม่คิดว่าลูกตัวเล็กๆ สามารถเข้าใจการนับจำนวนได้ตั้งแต่เมื่อไหร่คะ&nbsp;</span></h2>
<p><span style="color: rgb(28, 30, 33);">งานวิจัยจาก มหาวิทยาลัย Johns Hopkins ประเทศสหรัฐอเมริกา ตีพิมพ์ในวารสาร Science Developmental ระบุว่า เด็กทารกวัย 14 เดือน มีความเข้าใจการนับจำนวน แม้ว่าจะยังไม่เข้าใจความหมายทั้งหมดก็ตาม&nbsp;&nbsp;</span></p>
<p><br></p>
<p><span style="color: rgb(28, 30, 33);">นักวิจัยคัดเลือกเด็กอายุระหว่าง 13 ถึง 20 เดือน จำนวน&nbsp;80 คน ทำการทดสอบด้วยวิธีซ่อนของเล่นอย่างสุนัขหรือรถยนต์ในกล่องทึบเป็นจำนวน 5 ครั้ง ด้วยกัน</span></p>
<p><br></p>
<p><span style="color: rgb(28, 30, 33);">นักวิจัยทดลองโดยการ นับจำนวนชิ้นของเล่นที่นำไปซ่อนในกล่อง โดยให้เด็กทารกมองแล้วออกเสียงการนับดังๆ เช่น สุนัขตัวที่ 1 สุนัขตัวที่ 2 สุนัขตัวที่ 3 สุนัขตัวที่ 4 สลับกับการพูดว่า นี่ นี่ นี่ และนี่คือสุนัขทั้ง 4 ตัว&nbsp;</span></p>
<p><br></p>
<p><span style="color: rgb(28, 30, 33);">ผลปรากฏว่าเด็กมีแนวโน้มที่จะไม่สนใจเมื่อไม่ได้มีการนับจำนวนของของเล่นก่อนถูกซ่อน แต่เมื่อนักวิจัยนับของเล่นเด็กๆ จะจำจำนวนของเล่นและพยายามมองหาของเล่นในกล่องให้ครบตามจำนวนที่นักวิจัยนับให้เห็นก่อนหน้า</span></p>
<p><br></p>
<p><span style="color: rgb(28, 30, 33);">Jenny Wang ผู้ร่วมวิจัยกล่าวว่า "เมื่อเรานับของเล่น ก่อนจะเอาไปซ่อนเด็กๆ จะจำได้ดีว่ามีของเล่นกี่ชิ้น น่าประหลาดใจจริงๆ ที่เด็กอายุน้อยสามารถเชื่อมโยงคำศัพท์กับปริมาณที่สอดคล้องกันได้"&nbsp;</span></p>
<p><br></p>
<p><span style="color: rgb(28, 30, 33);">นอกจากนี้พวกเขายังแนะนำว่าการให้ลูกวัยทารกอ่านหนังสือเกี่ยวกับตัวเลขและการนับจำนวน จะช่วยให้พวกเด็กๆ เข้าใจเรื่องการการนับได้ดีก่อนวัยเรียนด้วยค่ะ</span></p>'
],
[
'title'='สไลด์เดอร์เด็กช่วยเสริมพัฒนาการสมองและกล้ามเนื้อ',
'img'='/images/article/3.jpg',
'content'='<p>แม้ว่า สไลด์เดอร์เด็ก จะดูเป็นของเล่นธรรมดาๆ แต่การเล่นสไลด์เดอร์กลับช่วยส่งเสริมพัฒนาการทางสมอง สายตา อารมณ์ กล้ามเนื้อ อีกทั้งทักษะสำคัญที่จะทำให้เด็กก้าวไปสู่การเป็นผู้ใหญ่ที่ประสบความสำเร็จในชีวิต มาดูกันในรายละเอียดเลยดีกว่า</p>
<p><br></p>
<p><strong>1. การกะระยะ</strong>&nbsp;ในการกะระยะ เด็กๆ จะต้องใช้สายตาเพื่อมองดูว่าเขาจะต้องเอื้อมมือไปจับตรงไหนหรือวางเท้าที่ตรงไหนจึงจะทำให้ตัวเองสามารถปีนขึ้นไปได้ การกะระยะคือการประสานความเชื่อมโยงระหว่างสายตา สมองส่วนหน้าและเปลือกสมองให้ทำงานร่วมกับกล้ามเนื้ออย่างมีประสิทธิภาพ ส่งผลให้ลูกของคุณเป็นเด็กที่สามารถทำงานต่างๆ เช่น งานประดิษฐ์ หรือเล่นกีฬาได้ดีกว่าเด็กที่ไม่ได้รับการฝึกฝนเกี่ยวกับการกะระยะเลย</p>
<p><strong>2. การตั้งเป้าหมาย</strong>&nbsp;สิ่งหนึ่งที่จะทำให้ลูกของคุณโตไปเป็นผู้ใหญ่ที่ประสบความสำเร็จก็คือการพัฒนา Exclusive Function หรือ EF การที่เด็กรู้จักตั้งเป้าหมายและมุ่งมั่นทำเป้าหมายนั้นให้สำเร็จ นั่นแหละคือ Exclusive function ที่สำคัญ หากตอน 3 ขวบ ลูกของคุณตั้งเป้าได้ว่าจะปีนไปให้ถึงขั้นบนสุดของบันไดและมุ่งมั่นทำจนสำเร็จได้ พออายุ 20 เขาก็ตั้งเป้าได้เหมือนกันว่าอยากจะทำอะไรหรือเป็นอะไร และรู้จักที่จะอดทนฝ่าฟันจนกว่าจะไปถึงเป้าหมายนั้น</p>
<p><strong>3. อารมณ์</strong>&nbsp;พัฒนาการทางด้านอารมณ์เป็นสิ่งที่สำคัญมาก เวลาที่เด็กลื่นไถลลงมาจากสไลด์เดอร์ เด็กๆ มักจะยิ้มและส่งเสียงหัวเราะ การที่เด็กมีอารมณ์ดีสนุกสนานร่าเริงเป็นประจำจนถึงวัยตัดแต่งประสาท ระบบประสาทจะคงเส้นประสาทแห่งความร่าเริงเหล่านี้เอาไว้ ส่งผลให้เขาโตไปเป็นผู้ใหญ่ที่มองโลกในแง่บวก รู้จักเข้าสังคมและไม่ต่อต้านสังคม</p>
<p><strong>4. ส่งเสริมความเคารพในตัวเอง</strong>&nbsp;เวลาที่เด็กได้รับความไว้วางใจจากคุณพ่อคุณแม่ เขาก็จะเชื่อมั่นในตัวเองและมีความเคารพในตัวเองด้วย หากคุณมี สไลด์เดอร์เด็ก ที่บ้านและคุณได้ใช้แผ่นโฟมปูรองพื้นบริเวณสไลด์เดอร์จนสไลด์เดอร์ของคุณมีความปลอดภัยพอสมควร เวลาที่ลูกปีนขึ้นไปบนสไลด์เดอร์ คุณก็แค่ยืนมองอยู่ห่างๆ ทำให้เขารู้ว่าคุณไว้ใจเขา ไม่ว่าเขาจะวิ่งทวนขึ้นสไลด์โดยไม่ใช้บันได หรือนอนเอา</p>
<p>หัวสไลด์ลงมา คุณก็ไม่ต้องร้องห้ามว่าอย่าเอาหัวลงนะลูก หรือใช้บันไดซิลูก แค่คุณไว้ใจเขา เขาก็จะไว้ใจตัวเอง ทำทุกอย่างให้ตัวเองปลอดภัย สิ่งนี้จะนำไปสู่การเป็นผู้ใหญ่ที่เคารพตัวเอง เคารพในการตัดสินใจของตัวเองและประสบความสำเร็จในหน้าที่การงานต่อไป</p>
<p>เห็นหรือยังว่าของเล่นธรรมดาๆ อย่าง สไลด์เดอร์เด็ก มีความไม่ธรรมดาอยู่ในตัว มันสามารถส่งเสริมพัฒนาการของลูกน้อยของคุณได้อย่างไม่น่าเชื่อ</p>'
],
[
'title'='ลูกรอดตายเพราะคาร์ซีท',
'img'='/images/article/4.jpg',
'content'='<p><strong>คาร์ซีท</strong>หรือเบาะนั่งนิรภัยในรถยนต์สำหรับเด็ก คืออุปกรณ์ที่สำคัญมาก สำหรับเด็ก ซึ่งควรใช้ตั้งแต่แรกเกิด ในบางประเทศถึงกับออกกฎหมายบังคับ พ่อแม่ทุกคนต้องให้ลูกนั่ง<strong>คาร์ซีท</strong>ตั้งแต่ออกจากโรงพยาบาล&nbsp;<strong>คาร์ซีท</strong>จะช่วยปกป้องลูกน้อยระหว่างการเดินทางโดยรถยนต์&nbsp;ในปัจจุบันประเทศไทยก็เริ่มให้ความสำคัญเรื่อง<strong>คาร์ซีท</strong>มากขึ้น ถึงแม้จะยังไม่มีกฎหมายบังคับใช้ และ<strong>คาร์ซีท</strong>มีราคาค่อนข้างแพง</p>
<p>วันนี้เรามีประสบการณ์จริง จากประโยชน์ของการใช้<strong>คาร์ซีท</strong>&nbsp;ที่คุ้มค่ามากเท่าชีวิต</p>
<p>คุณปีใหม่ คุณแม่มือใหม่ที่ศึกษาหาข้อมูลเกี่ยวกับ<strong>คาร์ซีท</strong>เมื่อเริ่มตั้งครรภ์ เธอให้ลูกนั่ง<strong>คาร์ซีท</strong>ตั้งแต่ออกจากโรงพยาบาลในวันแรก ช่วงนั้นค่อนข้างมีปัญหากับทางบ้านนิดหน่อย เนื่องจากคุณแม่ของเธอเป็นคนหัวโบราณ ไม่เข้าใจเรื่อง<strong>คาร์ซีท</strong>&nbsp;และคิดว่าจะอุ้มหลานเอง อุ้มเองก็ต้องปลอดภัยอยู่แล้ว ซึ่งเป็นความคิดที่ผิด เธอจึงต้องอธิบายให้คุณแม่เข้าใจอยู่หลายครั้ง แต่ก็ไม่ค่อยเป็นผลเท่าไหร่</p>
<p>จนมาวันหนึ่งเกิดเหตุการณ์ไม่คาดฝันขึ้น เธอเล่าว่า วันนั้นเธอ สามีและลูก กำลังเดินทางกลับจากจังหวัดชลบุรี&nbsp;อยู่บนทางด่วนบูรพาวิถี สามีใช้ความเร็วปกติ เธอนั่งที่เบาะหลัง ส่วนน้องบีน่าลูกสาววัยเพียง 2 เดือน นอนหลับปุ๋ยอยู่ใน<strong>คาร์ซีท</strong>&nbsp;เมื่อสามีขับออกมาจากช่องเก็บค่าทางด่วน สังเกตเห็นว่ารถคันหน้าที่ขับอยู่เลนขวา ขับช้าผิดปกติ สามีจึงจะแซงซ้ายขึ้นไป ทันใดนั้นรถคันหน้าก็เปลี่ยนเลนมาทางซ้ายกะทันหัน เลยชนเข้าอย่างแรง</p>
<p><img src="http://ailebebethailand.com/wp-content/uploads/2017/07/%E0%B8%AD%E0%B8%B8%E0%B8%9A%E0%B8%B1%E0%B8%95%E0%B8%B4%E0%B9%80%E0%B8%AB%E0%B8%95%E0%B8%B81-800x600.jpg" height="375" width="500"></p>
<p>เมื่อรู้ตัวอีกทีตัวเธอกระเด็นไปอยู่ที่เบาะหน้ามีรอยฟกช้ำ ระบมไปทั้งตัว&nbsp;Airbag แตกรอบคัน พอมีสติก็รีบเปิดประตูรถไปดูน้องบีน่าก่อนเลย น้องบีน่ายังหลับปุ๋ยอยู่ใน<strong>คาร์ซีท</strong>เหมือนเดิม ไม่มีร่องรอยบาดเจ็บใดๆ&nbsp;นี่ถ้าอุ้มลูกไว้เองจะเป็นยังไงไม่อยากนึกเลย ตัวเองยังเอาไม่รอด ขอบคุณ<strong>คาร์ซีท</strong>มากๆ ลูกรอดตายเพราะ<strong>คาร์ซีท</strong>จริงๆ</p>
<p>หลังจากเหตุการณ์นี้ คุณแม่ของเธอก็เข้าใจประโยชน์ของ<strong>คาร์ซีท</strong>แล้วว่าสำคัญมากเพียงใด&nbsp;<strong>ในวันนั้นคาร์ซีท</strong>ได้ทำหน้าที่ปกป้องชีวิตหลานตัวน้อยเอาไว้ได้อย่างสมบูรณ์ที่สุด&nbsp;และได้นำเรื่องนี้ไปบอกต่อ กับคนรู้จักว่า “หลานฉันรอดมาได้เพราะ<strong>คาร์ซีท</strong>แท้ๆ”</p>
<p><a href="http://ailebebethailand.com/wp-content/uploads/2017/07/Screenshot_3.jpg" rel="noopener noreferrer" target="_blank" style="color: rgb(98, 127, 154);"><img src="http://ailebebethailand.com/wp-content/uploads/2017/07/Screenshot_3.jpg" height="322" width="431"></a></p>
<p>คุณปีใหม่ได้ถ่ายทอดเรื่องราวให้ทางทีมงาน Ailebebe Thailand ได้ทราบ ทางเราจึงได้นำ<strong>คาร์ซีท</strong>ตัวใหม่ไปเปลี่ยนให้เธอ และน้องบีน่าถึงที่บ้าน ซึ่งเป็นบริการหลังการขายของเรา คาร์ซีท Ailebebe เอ-เล-เบ-เบ รับประกันความปลอดภัย เปลี่ยน<strong>คาร์ซีท</strong>ตัวใหม่ให้ทันทีหากเกิดเหตุการณ์ไม่คาดฝัน</p>
<p>ส่วน<strong>คาร์ซีท</strong>ตัวเก่าที่ผ่านการเกิดประสบอุบัติเหตุมาแล้ว ทางเราจะเก็บมาเพื่อทำลายทิ้ง เพราะระบบภายในอาจมีรอยแตกหัก ซึ่งจะส่งผลให้เกิดอันตรายต่อลูกน้อยได้</p>
<p><a href="http://ailebebethailand.com/wp-content/uploads/2017/07/Screenshot_4.jpg" rel="noopener noreferrer" target="_blank" style="color: rgb(98, 127, 154);"><img src="http://ailebebethailand.com/wp-content/uploads/2017/07/Screenshot_4.jpg" height="241" width="504"></a></p>
<p class="ql-align-center">** หวังว่าเรื่องราวนี้จะเป็นประโยชน์กับคุณพ่อคุณแม่ทุกท่านที่รักลูกสุดหัวใจ **</p>
<p><br></p>'
],
[
'title'='การเลือกรถเข็นเด็ก เลี้ยงลูกตามใจหมอ',
'img'='/images/article/5.jpg',
'content'='<p class="ql-align-center"><a href="http://www.babygiftretail.com/wp-content/uploads/2017/11/%E0%B9%81%E0%B8%99%E0%B8%B0%E0%B8%99%E0%B8%B3%E0%B8%A3%E0%B8%96%E0%B9%80%E0%B8%82%E0%B9%87%E0%B8%99%E0%B9%80%E0%B8%94%E0%B9%87%E0%B8%81-02.png" rel="noopener noreferrer" target="_blank" style="color: rgb(98, 127, 154);"><img src="http://www.babygiftretail.com/wp-content/uploads/2017/11/%E0%B9%81%E0%B8%99%E0%B8%B0%E0%B8%99%E0%B8%B3%E0%B8%A3%E0%B8%96%E0%B9%80%E0%B8%82%E0%B9%87%E0%B8%99%E0%B9%80%E0%B8%94%E0%B9%87%E0%B8%81-02.png" height="600" width="485"></a></p>
<p><strong>รถเข็นเด็กยี่ห้อไหน</strong>&nbsp;ที่หมอเด็กเลือกให้ลูกตัวเอง โดยหมอวิน เพจ เลี้ยงลูกตามใจหมอ</p>
<p><a href="https://business.facebook.com/hashtag/%E0%B8%A3%E0%B8%96%E0%B9%80%E0%B8%82%E0%B9%87%E0%B8%99%E0%B9%80%E0%B8%94%E0%B9%87%E0%B8%81" rel="noopener noreferrer" target="_blank" style="color: rgb(0, 0, 255);">#รถเข็นเด็ก</a>&nbsp;และ&nbsp;<a href="https://business.facebook.com/hashtag/%E0%B8%AA%E0%B8%B4%E0%B9%88%E0%B8%87%E0%B8%9E%E0%B8%B6%E0%B8%87%E0%B8%81%E0%B8%A3%E0%B8%B0%E0%B8%97%E0%B8%B3" rel="noopener noreferrer" target="_blank" style="color: rgb(0, 0, 255);">#สิ่งพึงกระทำ</a></p>
<p><a href="https://business.facebook.com/hashtag/%E0%B8%AD%E0%B8%B8%E0%B8%9B%E0%B8%81%E0%B8%A3%E0%B8%93%E0%B9%8C%E0%B8%A2%E0%B8%B1%E0%B8%87%E0%B8%8A%E0%B8%B5%E0%B8%9E%E0%B8%AA%E0%B8%B3%E0%B8%AB%E0%B8%A3%E0%B8%B1%E0%B8%9A%E0%B9%81%E0%B8%A1%E0%B9%88%E0%B8%AA%E0%B8%B2%E0%B8%A2%E0%B8%8A%E0%B8%B4%E0%B8%A5" rel="noopener noreferrer" target="_blank" style="color: rgb(0, 0, 255);">#อุปกรณ์ยังชีพสำหรับแม่สายชิล</a></p>
<p>บอกก่อนเลยว่า&nbsp;<strong>รถเข็นเด็ก</strong>&nbsp;นั้นเป็นอุปกรณ์ที่ไม่ได้จำเป็นครับ หากมองในแง่ของการเลี้ยงดูและความปลอดภัย ไม่เหมือนคาร์ซีท ที่จำเป็นมาก ๆ ๆ (ไม้ยมก … ล้านตัว)</p>
<p>แต่&nbsp;<strong>“รถเข็นเด็ก”</strong>&nbsp;ก็เป็น gadget ที่ยากจะปฏิเสธ เพราะมันทำให้ชีวิตเราง่ายขึ้นเยอะมากครับ แต่ถ้าใครเป็นสายอุ้ม … อุ้มโลดจ้ะ แต่มันก็จะเมื่อยถึงเมื่อยมาก จริง ๆ… ดังนั้นเลือก<strong>รถเข็นเด็ก</strong>ที่เหมาะกับเงินในกระเป๋า และความสบายตัวของลูกละกันครับ</p>
<p class="ql-align-center"><a href="http://www.babygiftretail.com/wp-content/uploads/2017/11/%E0%B9%81%E0%B8%99%E0%B8%B0%E0%B8%99%E0%B8%B3%E0%B8%A3%E0%B8%96%E0%B9%80%E0%B8%82%E0%B9%87%E0%B8%99%E0%B9%80%E0%B8%94%E0%B9%87%E0%B8%81-03.png" rel="noopener noreferrer" target="_blank" style="color: rgb(98, 127, 154);"><img src="http://www.babygiftretail.com/wp-content/uploads/2017/11/%E0%B9%81%E0%B8%99%E0%B8%B0%E0%B8%99%E0%B8%B3%E0%B8%A3%E0%B8%96%E0%B9%80%E0%B8%82%E0%B9%87%E0%B8%99%E0%B9%80%E0%B8%94%E0%B9%87%E0%B8%81-03-800x794.png" height="794" width="800"></a></p>
<p><strong style="color: rgb(51, 51, 51);">รถเข็นเด็ก&nbsp;</strong><span style="color: rgb(51, 51, 51);">…ประโยชน์</span></p>
<ul>
    <li>ทำให้ลูกสามารถหลับได้ยามง่วงตอนไปนอกบ้าน</li>
    <li>ทำให้พ่อแม่ไม่เมื่อยมืออุ้มในเด็กเล็ก เวลาลูกเมื่อยและเหนื่อยในวัยเดินได้</li>
    <li>เราสามารถพาลูกไปเที่ยวได้และมั่นใจว่าลูกจะไม่วิ่งจู๊ดไปไหนจนเกิดอุบัติเหตุครับ&nbsp;… เพราะถูกล็อคเบื้องต้นบน<strong>รถเข็นเด็ก</strong>&nbsp;ทำให้ดูแลง่าย</li>
    <li>ที่เด็ดที่สุดในความคิดของภรรยาหมอ คือ เอาไว้วางถุงช้อปปิ้งด้วย 555 … ไม่เมื่อยมือดี</li>
</ul>
<p>แต่อย่างไรก็ตาม … มีการรายงานว่า สามารถเกิดอุบัติเหตุได้หากใช้<strong>รถเข็นเด็ก</strong>แบบไม่เหมาะสมครับ มีหลายเคสที่มาด้วยรถคว่ำหรือไหลจากที่สูงจนคว่ำทำให้หัวลูกกระแทกพื้นได้ครับ …&nbsp;จน AAP หรือสมาคมกุมารเวชศาสตร์ของประเทศสหรัฐอเมริกาต้องออกมา<strong>แนะนำรถเข็นเด็ก</strong>&nbsp;ว่าสิ่งที่พ่อแม่พึงกระทำยามใช้<strong>รถเข็นเด็ก</strong>ครับ……นั่นคือ</p>
<ol>
    <li>ใช้สายรัดทุกครั้งเมื่อวางเด็กใน<strong>รถเข็นเด็ก</strong>&nbsp;ตามคู่มือของ<strong>รถเข็นเด็ก</strong>ชนิดนั้น ๆ</li>
    <li>กรุณาใส่ของใช้ เช่น ผ้าอ้อม กระเป๋า ที่ตระกร้าใต้รถ “อย่าแขวนไว้ตรงแฮนด์รถเข็น” เพราะอาจทำให้รถพลิกคว่ำได้</li>
    <li>ใส่เบรคทุกครั้งที่จอด เพื่อป้องกันไม่ให้รถไหล</li>
    <li>ใช้งานตามอายุและน้ำหนักที่แนะนำของ<strong>รถเข็นเด็ก</strong>แต่ละรุ่นแต่ละยี่ห้อ</li>
    <li>อย่าใช้<strong>รถเข็นเด็ก</strong>เป็นเตียงนอนในเด็กอายุต่ำกว่า 4 เดือนครับ เพราะเด็กยังตัวเล็กมาก และสามารถเลื่อนตัวจนทำให้จมูกถูกอุด จนหายใจไม่ออกได้ครับข้อ 6-10 พ่อหมอเพิ่มเติมเอง ตามการใช้งานจริงของรถเข็นที่บ้านครับ เพื่อความสะดวกละครับ (1-5 เป็นคำแนะนำของอเมริกา)</li>
    <li>ปรับนอนราบได้ บางรุ่นปรับได้เกือบราบ (170 องศา) โดยเฉพาะในเด็กแรกเกิดเพราะคอยังไม่แข็งแรง นอนง่าย และ หลับสบาย</li>
    <li>มีแผ่นรองคอและรองหลัง เป็นสิ่งที่ดีครับสำหรับเด็กแรกเกิด จะป้องกันไม่ให้หัวและสันหลังโงกเงกครับ เพราะคอยังไม่แข็ง บางยี่ห้อให้มาเลย บางยี่ห้อไม่มี ต้องไปซื้อเพิ่มมาเองครับ ลองดู พ่อหมอว่า มีก็ดีครับ</li>
    <li>หมุนได้สี่ล้อ … กลับหน้าหลังได้ อันนี้เพื่อความสะดวกสบายของคนเข็นครับ และเข็นได้ลื่นกว่า …ส่วนที่ควรกลับหน้าหลังได้ก็เพื่อให้ลูกเห็นหน้าแม่ แม่มองหน้าลูกได้ตอนเข็นครับ บางยี่ห้อกลับหน้าหลังไม่ได้ บางยี่ห้อใช้วิธียกส่วนที่นอนกลับหน้าหลังเอง บางยี่ห้อแค่โยกด้ามจับเข็นก็กลับได้แล้ว</li>
    <li>น้ำหนักควรเบา เพราะถ้าหนัก คุณแม่จะยกเข้าออกรถได้ยากครับ ที่พ่อหมอใช้หรือเพื่อน ๆ ใช้กันจะน้อยกว่า 6-7 กิโล ไม่ควรเกินนี้ หากเยอะกว่านี้ยกยากไปละ กล้ามขึ้น 555</li>
    <li>เพื่อความชิลของเบบี๋ของเรา&nbsp;อาจจะมีออปชั่นอื่น ๆ เพิ่มอีก เช่น</li>
</ol>
<ul>
    <li>high chair คือ ระดับของรถสูง &gt;50 ซม. ลดฝุ่นและสิ่งสกปรกจากพื้นได้</li>
    <li>มีหลังคาคลุมกันแสงแดดและ UV</li>
    <li>น้ำหนักเบา</li>
    <li>โครงสร้างโปร่งสบาย ระบายอากาศได้ดี เพื่อความเย็นสบายของลูกครับ … เพราะบ้านเราเป็นเมืองร้อนเนอะ&nbsp;ฯลฯ</li>
</ul>
<p><a href="http://www.babygiftretail.com/wp-content/uploads/2017/11/%E0%B9%81%E0%B8%99%E0%B8%B0%E0%B8%99%E0%B8%B3%E0%B8%A3%E0%B8%96%E0%B9%80%E0%B8%82%E0%B9%87%E0%B8%99%E0%B9%80%E0%B8%94%E0%B9%87%E0%B8%81-04.png" rel="noopener noreferrer" target="_blank" style="color: rgb(98, 127, 154);"><img src="http://www.babygiftretail.com/wp-content/uploads/2017/11/%E0%B9%81%E0%B8%99%E0%B8%B0%E0%B8%99%E0%B8%B3%E0%B8%A3%E0%B8%96%E0%B9%80%E0%B8%82%E0%B9%87%E0%B8%99%E0%B9%80%E0%B8%94%E0%B9%87%E0%B8%81-04-800x797.png" height="797" width="800"></a></p>
<p>และทิ้งท้ายครับ ปัจจุบันพ่อแม่หลายคนนิยม&nbsp;<strong>รถเข็นเด็ก</strong>แบบพับเล็ก ๆ ที่จับเป็นแบบก้าน ซึ่งดีในแง่การเดินทางและเอาขึ้นเครื่องเนอะ แต่หมอขอแนะนำแบบนี้ครับ</p>
<p><strong>รถเข็นเด็ก</strong>พับเล็กไม่เหมาะกับเด็กแรกเกิดครับ เพราะไม่มีที่รองคอ และที่นั่งมักทำให้เด็ก fix กับที่ไม่ค่อยได้ครับ รอโตกว่านี้ตอนจะไปเที่ยวต่างประเทศค่อยว่ากันครับ แต่หากลูกยังเล็กแนะนำอันที่แข็งแรงมีที่รองคอดีกว่าเนอะ …</p>
<p><br></p>'
],