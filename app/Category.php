<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Category extends Model
{
    use \Znck\Eloquent\Traits\BelongsToThrough;

    // public function type()
    // {
    //     return $this->belongsTo('App\CategoryType',  'category_type_id');
    // }

    protected $fillable = ['name', 'description', 'img_url', 'slug', 'group'];

    public function products()
    {
        return $this->HasManyThrough('App\Product', 'App\ProductModel');
    }

    protected $appends = ['prices', 'code'];

    public function getCodeAttribute()
    {
        return substr($this->group, 0, 1) . substr($this->slug, 0, 1);
    }
    public function getPricesAttribute()
    {
        if ($this->group == 'Stroller') {
            return array(
                0 =>
                array(
                    'id' => 5,
                    'description' => '7 วัน',
                    'price' => '800',
                    'range' => '0',
                    'day' => 7
                ),
                1 =>
                array(
                    'id' => 6,
                    'description' => '15 วัน',
                    'price' => '1500',
                    'range' => '7',
                    'day' => 15

                ),
                2 =>
                array(
                    'id' => 7,
                    'description' => '30 วัน',
                    'price' => '2500',
                    'range' => '15',
                    'day' => 30

                ),
                3 =>
                array(
                    'id' => 8,
                    'description' => '90 วัน',
                    'price' => '3000',
                    'range' => '30',
                    'day' => 90

                ),
                4 =>
                array(
                    'id' => 9,
                    'description' => '180 วัน',
                    'price' => '3500',
                    'range' => '90',
                    'day' => 180

                ),
            );
        }



        if ($this->group == 'Carseat') {
            return array(
                0 =>
                array(
                    'id' => 5,
                    'description' => '7 วัน',
                    'price' => '700',
                    'range' => '0',
                    'day' => 7

                ),
                1 =>
                array(
                    'id' => 6,
                    'description' => '15 วัน',
                    'price' => '1400',
                    'range' => '7',
                    'day' => 15

                ),
                2 =>
                array(
                    'id' => 7,
                    'description' => '30 วัน',
                    'price' => '2400',
                    'range' => '15',
                    'day' => 30


                ),
                3 =>
                array(
                    'id' => 8,
                    'description' => '90 วัน',
                    'price' => '2900',
                    'range' => '30',
                    'day' => 90

                ),
                4 =>
                array(
                    'id' => 9,
                    'description' => '180 วัน',
                    'price' => '3400',
                    'range' => '90',
                    'day' => 180

                ),
            );
        }




        if ($this->group == 'Toy') {
            return array(
                0 =>
                array(
                    'id' => 6,
                    'description' => '15 วัน',
                    'price' => '1600',
                    'range' => '7',
                    'day' => 7

                ),
                1 =>
                array(
                    'id' => 7,
                    'description' => '30 วัน',
                    'price' => '2600',
                    'range' => '15',
                    'day' => 30

                ),
                2 =>
                array(
                    'id' => 8,
                    'description' => '90 วัน',
                    'price' => '3100',
                    'range' => '30',
                    'day' => 90

                ),
                3 =>
                array(
                    'id' => 9,
                    'description' => '180 วัน',
                    'price' => '3600',
                    'range' => '90',
                    'day' => 180

                ),
            );
        }
    }
}