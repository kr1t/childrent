<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $fillable = ['user_id',  'product_id', 'pickup_date', 'return_date'];

    protected $appends = ['price', 'price_txt'];
    protected $casts = [
        'pickup_date'  => 'date:Y-m-d',
        'return_date' => 'date:Y-m-d',
    ];

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function getPriceAttribute()
    {
        $date1 = date_create($this->pickup_date);
        $date2 = date_create($this->return_date);

        //difference between two dates
        $diff = date_diff($date1, $date2);

        $pricetxt = 0;
        //count days
        $count = $diff->format("%a");

        foreach ($this->product->category->prices as $price) {
            if ($count == intval($price['day'])) {
                $pricetxt = $price['price'];
            }
        }

        return  intval($pricetxt);
    }

    public function getPriceTxtAttribute()
    {
        $date1 = date_create($this->pickup_date);
        $date2 = date_create($this->return_date);

        //difference between two dates
        $diff = date_diff($date1, $date2);

        $pricetxt = 0;
        //count days
        $count = $diff->format("%a");
        foreach ($this->product->category->prices as $price) {
            if ($count == intval($price['day'])) {
                $pricetxt = $price['description'];
            }
        }

        $this->product->product_model;
        return $pricetxt;
    }

    // public function amount()
    // {
    //     return $this->belongsTo('App\ProductAmount');
    // }
}