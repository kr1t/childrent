<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductMark extends Model
{
    protected $fillable = ['image_url', 'product_id', 'des'];
}