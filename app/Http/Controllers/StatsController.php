<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseDetail;
use Carbon\Carbon;

Carbon::setLocale('th');

class StatsController extends Controller
{
    public function index()
    {
        $pd = PurchaseDetail::all();
        $num = $pd->groupBy(function ($date) {
            return Carbon::parse($date->created_at)->format('Y-M-d'); // grouping by date
        })->map(function ($row) {
            return $row->sum('price_all');
        });
        if (!$num) {
            return false;
        }
        $i = 0;
        $datax = false;
        foreach ($num as $key => $n) {
            $datax[$i]['วันที่'] = $key;
            $datax[$i]['รายได้(บาท)'] = $n;
            $i++;
        }

        $attributes = array_keys($num->toArray());
        //  ถอด key ออกมา

        return [$num, "rows" => $datax, 'columns' => ["วันที่", "รายได้(บาท)"]];
    }
}