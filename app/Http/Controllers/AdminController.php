<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;

class AdminController extends Controller
{
    public function addNews(Request $request)
    {
        return News::create($request->all());
    }

    public function editNews(Request $request, $id)
    {
        $news = News::find($id);
        $news->update($request->all());
        return $news;
    }

    public function miniNews(Request $request)
    {

        $news = News::orderBy('created_at', 'desc')->limit(3)->latest()->get();
        foreach ($news as $key => $new) {
            $new->content = strip_tags(preg_replace("/<img[^>]+\>/i", " ", $new->content));

            $string = strip_tags($new->content);
            if (strlen($string) > 300) {

                // truncate string
                $stringCut = substr($string, 0, 300);
                $endPoint = strrpos($stringCut, ' ');

                //if the string doesn't contain any space then it will cut without word basis.
                $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                $string .= '... อ่านต่อ';
            }

            $new->content =  $string;
        }
        return $news;
    }
    public function news(Request $request)
    {

        $news = News::orderBy('created_at', 'desc')->latest()->get();
        foreach ($news as $key => $new) {
            $new->content = strip_tags(preg_replace("/<img[^>]+\>/i", " ", $new->content));

            $string = strip_tags($new->content);
            if (strlen($string) > 300) {

                // truncate string
                $stringCut = substr($string, 0, 300);
                $endPoint = strrpos($stringCut, ' ');

                //if the string doesn't contain any space then it will cut without word basis.
                $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                $string .= '... อ่านต่อ';
            }

            $new->content =  $string;
        }
        return $news;
    }
    public function newsShow($id)
    {
        $news = News::find($id);
        //select * from news where id = $id
        $recenet = News::where('id', '!=', $id)->latest()->get()->take(4);

        foreach ($recenet as $key => $new) {
            $new->content = strip_tags(preg_replace("/<img[^>]+\>/i", " ", $new->content));
            $string = strip_tags($new->content);
            if (strlen($string) > 200) {

                // truncate string
                $stringCut = substr($string, 0, 200);
                $endPoint = strrpos($stringCut, ' ');

                //if the string doesn't contain any space then it will cut without word basis.
                $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
                $string .= '... อ่านต่อ';
            }

            $new->content =  $string;
        }
        $news->recents =  $recenet;

        return $news;
    }
    public function newsdel($id)
    {
        $news = News::find($id);
        $news->delete();
        return $news;
    }
}