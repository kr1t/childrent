<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\ProductImage;
use App\ProductMark;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function recentModel(Request $request, $id)
    {
        $prod = Product::find($id);
        $products = Product::where('product_model_id', $prod->product_model_id)->where('id', '!=', $id)->get()->take(4);
        return $products;
    }

    public function allAdmin(Request $request)
    {

        if (!$request->del) {
            //  ไม่ส่ง ?del=1
            // ไม่ปลดระวาง
            $products = Product::where('name', 'like', '%' . $request->q . '%')->where(function ($query) use ($request) {
                if ($request->categories) {

                    // select * from products where IN (select * product_models where id = product.product_model_id and category IN 1,2,3)

                    $query->whereHas('product_model', function ($query)  use ($request) {
                        $query->whereIn('category_id', $request->categories);
                    });


                    //  [1,2,3]
                }
            })->whereNull('delete_at')->latest()->get();
        } else {
            // ปลดระวาง
            // ส่ง del=1

            // soft delete
            $products = Product::where('name', 'like', '%' . $request->q . '%')->where(function ($query) use ($request) {
                if ($request->categories) {
                    $query->whereHas('product_model', function ($query)  use ($request) {
                        $query->whereIn('category_id', $request->categories);
                    });
                }
            })->whereNotNull('delete_at')->latest()->get();
        }


        return $products;
    }
    public function all(Request $request)
    {

        if (!$request->del) {
            //  ไม่ส่ง ?del=1
            // ไม่ปลดระวาง
            $products = Product::where('name', 'like', '%' . $request->q . '%')->where('status', 1)->where(function ($query) use ($request) {
                if ($request->categories) {

                    // select * from products where IN (select * product_models where id = product.product_model_id and category IN 1,2,3)

                    $query->whereHas('product_model', function ($query)  use ($request) {
                        $query->whereIn('category_id', $request->categories);
                    });


                    //  [1,2,3]
                }
            })->whereNull('delete_at')->latest()->get();
        } else {
            // ปลดระวาง
            // ส่ง del=1

            // soft delete
            $products = Product::where('name', 'like', '%' . $request->q . '%')->where('status', 1)->where(function ($query) use ($request) {
                if ($request->categories) {
                    $query->whereHas('product_model', function ($query)  use ($request) {
                        $query->whereIn('category_id', $request->categories);
                    });
                }
            })->whereNotNull('delete_at')->latest()->get();
        }


        return $products;
    }
    public function index(Request $request)
    {


        if ($request->cat) {
            $products = Product::whereNull('delete_at')->where('status', 1)->whereHas('product_model', function ($query) use ($request) {
                $query->where('category_id', $request->cat);
            })->get()->take(4);
        } else {
            $products = Product::whereNull('delete_at')->where('status', 1)->get()->random(4);
        }

        if ($request->cat && $request->swaping) {

            $c1 = Category::find($request->cat);
            $c_all = Category::where('group', $c1->group)->get()->pluck('id');
            $products = Product::whereNull('delete_at')->where('status', 1)->whereHas('product_model', function ($query) use ($c_all) {
                $query->whereIn('category_id', $c_all);
            })->get();
        }


        foreach ($products as $product) {
            $product->price;
            $product->marks;
        }

        return $products;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $product = Product::create($request->all());
        foreach ($request->imgs as $img) {
            ProductImage::create([
                'file_url' => $img,
                'product_id' => $product->id
            ]);
        }
        foreach ($request->img_marks as $img) {
            ProductMark::create([
                'image_url' => $img,
                'product_id' => $product->id
            ]);
        }
        return $request->all();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $product;
        $product->marks;

        $product->category->type;
        // $product->amounts;
        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $product->update($request->all());

        ProductImage::where('product_id', $product->id)->delete();
        ProductMark::where('product_id', $product->id)->delete();

        foreach ($request->imgs as $img) {
            ProductImage::create([
                'file_url' => $img,
                'product_id' => $product->id
            ]);
        }
        foreach ($request->img_marks as $img) {
            ProductMark::create([
                'image_url' => $img,
                'product_id' => $product->id
            ]);
        }
        return $request->all();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {

        if (request()->undel == 1) {
            $set = Null;
            // ยกเลิกปลดระวาง
        } else {
            $set = Carbon::now();
            // เวลาปัจจุบัน
            // ปลดระวาง

        }

        $product->update([
            "delete_at" => $set
        ]);
        return $product;
    }
}