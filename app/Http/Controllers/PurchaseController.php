<?php

namespace App\Http\Controllers;

use App\Purchase;
use App\PurchaseDetail;
use App\Cart;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $pur = Purchase::whereUserId(request()->user()->id)->with('pickup')->with('return')->latest()->get();

        foreach ($pur as $p) {
            foreach ($p->details as $d) {
                $d->product;
            }
        }
        return $pur;
    }

    public function adminIndex(Request $request)
    {

        $pur = PurchaseDetail::with('purchase')->with('product')->with('purchase.pickup')->with('purchase.return')->with('purchase.user')->where('status', request()->status)->orderBy('pickup_date', 'asc')->get();

        if (request()->q) {
            $pur = PurchaseDetail::with('purchase')->with('product')->with('purchase.pickup')->with('purchase.return')->with('purchase.user')
                ->where('status', request()->status)

                // ->where(function ($query) {
                //     $query->where('id', 'like', '%' . request()->q . '%');
                // })
                ->whereHas('purchase', function ($query) use ($request) {
                    $query->where('uid', 'like', '%' . $request->q . '%');
                })


                // ->where(function ($query) {
                //     $query->whereHas('product', function ($query) {
                //         $query->where('name', '%' . request()->q . '%');
                //     });
                // })
                ->orderBy('pickup_date', 'asc')
                ->get();
        }
        foreach ($pur as $p) {
            $bef = PurchaseDetail::where('id', '!=', $p->id)->where('purchase_id', $p->purchase_id)->with('product')->latest()->first();
            $p->bef = $bef;
        }
        return $pur;
    }

    public function setStatus(Request $request)
    {

        $pd = PurchaseDetail::find($request->id);
        $pd->update([
            "status" => $request->status
        ]);


        $user = $pd->purchase->user;
        $sms = new THSMS();
        if ($request->status == 2) {
            $sms->send('0000', $user->mobile_number, 'สินค้า #' . $pd->purchase->uid . ' ของท่านถูกจัดส่งเรียบร้อย รายละเอียดคลิก http://bit.ly/38YjD3f');
        } else if ($request->status == 3) {
            $sms->send('0000', $user->mobile_number, 'ท่านได้รับสินค้า #' . $pd->purchase->uid . ' แล้ว รายละเอียดคลิก http://bit.ly/38YjD3f');
        } else if ($request->status == 4) {
            $sms->send('0000', $user->mobile_number, 'status4 #' . $pd->purchase->uid . ' แล้ว รายละเอียดคลิก http://bit.ly/38YjD3f');
        } else if ($request->status == 5) {
            $sms->send('0000', $user->mobile_number, 'status5 #' . $pd->purchase->uid . ' แล้ว รายละเอียดคลิก http://bit.ly/38YjD3f');
        }

        return 'ok';
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $carts = Cart::where('user_id', $request->user()->id)->get();


        foreach ($carts as $cart) {
            $pc = Purchase::create($request->all());
            $pc->update(
                [
                    "user_id" => $request->user()->id,
                    "pickup_date" => $cart->pickup_date,
                    "return_date" => $cart->return_date,
                    "uid" => $cart->product->product_model->code . '-' . $pc->id
                ]
            );



            $pd = PurchaseDetail::create([
                "purchase_id" => $pc->id,
                "product_id" => $cart->product_id,
                "pickup_date" => $cart->pickup_date,
                "return_date" => $cart->return_date,
                "status" => 1,
                "price_all" => $request->price_all,
            ]);

            $pd->product->update([
                "status" => 2
            ]);

            $cart->delete();
        }

        $user = $request->user();
        $sms = new THSMS();
        $sms->send('0000', $user->mobile_number, 'ขอบคุณ ที่ใช้บริการ Chilrent สินค้าของท่านจะถูกจัดส่งใน 2 วันทำการ รายละเอียดคลิก http://bit.ly/38YjD3f');
        return 'ok';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    // public function show(Purchase $purchase)
    // {
    //     return $p
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function edit(Purchase $purchase)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Purchase $purchase)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purchase  $purchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Purchase $purchase)
    {
        //
    }
}

class THSMS
{
    var $api_url   = 'http://www.thsms.com/api/rest';
    var $username  = 'armkrit';
    var $password  = 'helloworld';

    public function getCredit()
    {
        $params['method']   = 'credit';
        $params['username'] = $this->username;
        $params['password'] = $this->password;

        $result = $this->curl($params);

        $xml = @simplexml_load_string($result);

        if (!is_object($xml)) {
            return array(FALSE, 'Respond error');
        } else {

            if ($xml->credit->status == 'success') {
                return array(TRUE, $xml->credit->status);
            } else {
                return array(FALSE, $xml->credit->message);
            }
        }
    }

    public function send($from = '0000', $to = null, $message = null)
    {
        $params['method']   = 'send';
        $params['username'] = $this->username;
        $params['password'] = $this->password;

        $params['from']     = $from;
        $params['to']       = $to;
        $params['message']  = $message;

        if (is_null($params['to']) || is_null($params['message'])) {
            return FALSE;
        }

        $result = $this->curl($params);
        $xml = @simplexml_load_string($result);
        if (!is_object($xml)) {
            return array(FALSE, 'Respond error');
        } else {
            if ($xml->send->status == 'success') {
                return array(TRUE, $xml->send->uuid);
            } else {
                return array(FALSE, $xml->send->message);
            }
        }
    }

    private function curl($params = array())
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->api_url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response  = curl_exec($ch);
        $lastError = curl_error($ch);
        $lastReq = curl_getinfo($ch);
        curl_close($ch);

        return $response;
    }
}