<?php

namespace App\Http\Controllers;

use App\PackageMark;
use Illuminate\Http\Request;

class PackageMarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PackageMark  $packageMark
     * @return \Illuminate\Http\Response
     */
    public function show(PackageMark $packageMark)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PackageMark  $packageMark
     * @return \Illuminate\Http\Response
     */
    public function edit(PackageMark $packageMark)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PackageMark  $packageMark
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PackageMark $packageMark)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PackageMark  $packageMark
     * @return \Illuminate\Http\Response
     */
    public function destroy(PackageMark $packageMark)
    {
        //
    }
}
