<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseDetail;
use App\Purchase;

class Swap extends Controller
{

    public function unRent(Request $request)
    {
        $pd_bef = PurchaseDetail::where('purchase_id', $request->id)->latest()->first();

        $pd = PurchaseDetail::create(
            [
                'purchase_id' => $request->id,
                'product_id' => $pd_bef->product_id,
                'pickup_date' => $pd_bef->pickup_date,
                'return_date' => $pd_bef->return_date,
                'price_all' => 0,
                'status' => 5
            ]
        );

        $pd_bef->product->update([
            "status" => 4
        ]);
        $pd->purchase->update([
            "address_id" => $request->address_id,
            "address_return_id" => $request->address_id
        ]);
        return $pd_bef;
    }
    public function store(Request $request)
    {

        $pd_bef = PurchaseDetail::where('purchase_id', $request->purchase_id)->latest()->first();

        $return_date = PurchaseDetail::where('purchase_id', $request->purchase_id)->first()->pluck('return_date');
        $pd = PurchaseDetail::create(
            [
                'purchase_id' => $request->purchase_id,
                'product_id' => $request->product_id,
                'pickup_date' => $pd_bef->pickup_date,
                'return_date' => $pd_bef->return_date,
                'price_all' => $request->price_all,
                'status' => 4
            ]
        );

        $pd->product->update([
            "status" => 2
        ]);
        $pd_bef->product->update([
            "status" => 4
        ]);
        $pd->purchase->update([
            'address_id' => $request->address_id,
            'address_return_id' => $request->address_id,
        ]);

        return 'ok';
    }
}