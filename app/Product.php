<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Color;

class Product extends Model
{


    protected $fillable = ['status', 'name', 'deposit', 'youtube_url', 'color', 'price', 'product_model_id', 'delete_at'];
    protected $appends = ['range', 'img_url', 'colors', 'model', 'weight', 'category_id', 'description'];
    protected $casts = [
        'color' => 'Array'
    ];


    public function getRangeAttribute()
    {
        $from = explode('.', $this->product_model->from_age);
        $to = explode('.', $this->product_model->to_age);
        $from_year = $from[0] ? intval($from[0]) . 'ปี ' : '';
        $from_month = isset($from[1]) ? $from[1] ? intval($from[1]) . 'เดือน' : '' : '';
        $to_year = $to[0] ? intval($to[0]) . 'ปี ' : '';
        $to_month = isset($to[1]) ? $to[1] ? intval($to[1]) . 'เดือน' : '' : '';
        return $from_year . ' ' . $from_month . ' - ' . $to_year .  $to_month;
    }

    public function getModelAttribute()
    {
        return $this->product_model->name;
    }

    public function getDescriptionAttribute()
    {
        return $this->product_model->description;
    }


    public function getWeightAttribute()
    {
        return $this->product_model->weight;
    }

    public function getCategoryIdAttribute()
    {
        return $this->product_model->category_id;
    }



    public function getImgUrlAttribute()
    {
        return count($this->images) ? $this->images[0]->file_url : '';
    }

    public function getColorsAttribute()
    {
        $c = [];
        if ($this->color) {
            foreach ($this->color as $index => $color_id) {
                $checker = Color::find($color_id);

                // select * from colors where id = $color
                if ($checker) {
                    $c[$index] = $checker;
                }
            }
        }


        return $c;
    }

    // public function getPriceAttribute()
    // {
    //     return $this->prices[0]->price;
    // }
    // public function getAmountAttribute()
    // {

    //     return [
    //         "amount" => $this->amounts[0]->amount,
    //         "id" => $this->amounts[0]->id
    //     ];
    // }




    public function prices()
    {
        return $this->hasMany('App\ProductPrice');
    }

    public function product_model()
    {
        return $this->belongsTo('App\ProductModel');
    }

    public function marks()
    {
        return $this->hasMany('App\ProductMark');
    }


    // public function amounts()
    // {
    //     return [];
    // }

    public function images()
    {
        return $this->hasMany('App\ProductImage');
    }
    // public function price()
    // {
    //     return $this->hasOne('App\ProductPrice');
    // }

    use \Znck\Eloquent\Traits\BelongsToThrough;


    public function category()
    {
        return $this->belongsToThrough('App\Category', 'App\ProductModel');
    }
}