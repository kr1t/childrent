<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class PurchaseDetail extends Model
{
    protected $appends = ['date_diff'];
    protected $fillable = ['purchase_id', 'product_id', 'pickup_date', 'return_date', 'status', 'price_all'];
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
    public function purchase()
    {
        return $this->belongsTo('App\Purchase');
    }

    public function getDateDiffAttribute()
    {
        $cDate = Carbon::parse($this->return_date);
        return $cDate->diffInDays();
    }
}