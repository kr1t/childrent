<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ProductModel extends Model
{
    protected $fillable = ['name', 'code', 'brand_id', 'category_id', 'description', 'from_age', 'to_age', 'weight'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function getCodeAttribute($value)
    {
        return $this->category->code . $this->brand_id . $value  . 'PID' . $this->id . '-' . Carbon::now()->format('Ymd');
    }
}