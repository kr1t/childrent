<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purechase extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}