<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = ['user_id', 'address_id', 'address_return_id', 'status', 'uid'];

    public function details()
    {
        return $this->hasMany('App\PurchaseDetail')->latest();
    }
    public function pickup()
    {
        return $this->belongsTo('App\Address', 'address_id');
    }
    public function return()
    {
        return $this->belongsTo('App\Address', 'address_return_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}